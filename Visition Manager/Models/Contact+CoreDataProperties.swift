//
//  Contact+CoreDataProperties.swift
//  Visitation Manager
//
//  Created by Judah M Eddy on 1/26/21.
//  Copyright © 2021 Master Design. All rights reserved.
//
//

import Foundation
import CoreData


extension Contact {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Contact> {
        return NSFetchRequest<Contact>(entityName: "Contact")
    }

    @NSManaged public var address: String?
    @NSManaged public var age: String?
    @NSManaged public var city: String?
    @NSManaged public var firstName: String?
    @NSManaged public var lastName: String?
    @NSManaged public var lastPrint: Date?
    @NSManaged public var lastVisited: Date?
    @NSManaged public var nextPrint: Date?
    @NSManaged public var notes: String?
    @NSManaged public var phone: String?
    @NSManaged public var state: String?
    @NSManaged public var status: Int16
    @NSManaged public var zipCode: String?
    @NSManaged public var serverID: String?
    @NSManaged public var groups: NSSet?
    @NSManaged public var visits: NSOrderedSet?

}

// MARK: Generated accessors for groups
extension Contact {

    @objc(addGroupsObject:)
    @NSManaged public func addToGroups(_ value: Group)

    @objc(removeGroupsObject:)
    @NSManaged public func removeFromGroups(_ value: Group)

    @objc(addGroups:)
    @NSManaged public func addToGroups(_ values: NSSet)

    @objc(removeGroups:)
    @NSManaged public func removeFromGroups(_ values: NSSet)

}

// MARK: Generated accessors for visits
extension Contact {

    @objc(insertObject:inVisitsAtIndex:)
    @NSManaged public func insertIntoVisits(_ value: Visit, at idx: Int)

    @objc(removeObjectFromVisitsAtIndex:)
    @NSManaged public func removeFromVisits(at idx: Int)

    @objc(insertVisits:atIndexes:)
    @NSManaged public func insertIntoVisits(_ values: [Visit], at indexes: NSIndexSet)

    @objc(removeVisitsAtIndexes:)
    @NSManaged public func removeFromVisits(at indexes: NSIndexSet)

    @objc(replaceObjectInVisitsAtIndex:withObject:)
    @NSManaged public func replaceVisits(at idx: Int, with value: Visit)

    @objc(replaceVisitsAtIndexes:withVisits:)
    @NSManaged public func replaceVisits(at indexes: NSIndexSet, with values: [Visit])

    @objc(addVisitsObject:)
    @NSManaged public func addToVisits(_ value: Visit)

    @objc(removeVisitsObject:)
    @NSManaged public func removeFromVisits(_ value: Visit)

    @objc(addVisits:)
    @NSManaged public func addToVisits(_ values: NSOrderedSet)

    @objc(removeVisits:)
    @NSManaged public func removeFromVisits(_ values: NSOrderedSet)

}
