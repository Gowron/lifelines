//
//  DataManager.swift
//  Visition Manager
//
//  Created by Master Design on 4/11/19.
//  Copyright © 2019 Master Design. All rights reserved.
//

import Foundation
import CoreData

// MARK: - CONTACTS

extension Contact {
    
    static let edit = "EditContact"
    static let Sort_Name = 0
    static let Sort_City = 1
    static let Sort_Visit = 2
    
    static func createContact(withContext context: NSManagedObjectContext) -> Contact {
        let contact = Contact(context: context)
        if let group = Group.activeGroup(withContext: context) {
            contact.addToGroups(group)
        }
        // TODO: Set State to Default
        contact.nextPrint = Date()
        return contact
    }
    
    func archive() {
        if let context = self.managedObjectContext {
            if let active = Group.activeGroup(withContext: context) {
                if let archive = Group.archiveGroup(withContext: context) {
                    if self.groups?.contains(active) ?? false {
                        self.removeFromGroups(active)
                        self.addToGroups(archive)
                    } else if self.groups?.contains(archive) ?? false {
                        self.removeFromGroups(archive)
                        self.addToGroups(active)
                    }
                }
            }
        }
    }
}

// MARK: - VISITS

extension Visit {
    
    static func visit(forContact contact: Contact?, withContext context: NSManagedObjectContext) -> Visit {
        
        let visit = Visit(context: context)
        visit.contact = contact
        visit.date = Date()
        if let contact = contact {
            if visit.date!.compare(contact.lastVisited! as Date) == .orderedAscending {
                contact.lastVisited = visit.date!
            }
        }
        return visit
    }
}

// MARK: - GROUPS

extension Group {
    
    static let active = "Active Contacts"
    static let archive = "Archived Contacts"
    
    static func activeGroup(withContext context: NSManagedObjectContext) -> Group? {
        do {
            let fetchRequest: NSFetchRequest<Group> = Group.fetchRequest()
            fetchRequest.predicate = NSPredicate(format: "displayName == '\(active)'")
            let groups = try context.fetch(fetchRequest)
            if groups.count == 1 {
                return groups[0]
            }
        } catch {
            
        }
        return nil
    }
    
    static func archiveGroup(withContext context: NSManagedObjectContext) -> Group? {
        do {
            let fetchRequest: NSFetchRequest<Group> = Group.fetchRequest()
            fetchRequest.predicate = NSPredicate(format: "displayName == '\(archive)'")
            let groups = try context.fetch(fetchRequest)
            if groups.count == 1 {
                return groups[0]
            }
        } catch {
            
        }
        return nil
    }
    
    static func createPrintGroup(withContext context: NSManagedObjectContext) -> Group {
        let group = Group(context: context)
        do {
            let now = Date()
            let fetchRequest: NSFetchRequest<Contact> = Contact.fetchRequest()
            
            fetchRequest.predicate  = NSPredicate(format: "nextPrint < %@ || nextPrint == nil", now as NSDate)
            var contacts = try context.fetch(fetchRequest)
            if let archiveGroup = Group.archiveGroup(withContext: context) {
                for contact in contacts {
                    if archiveGroup.contacts?.contains(contact) ?? true {
                        if let index = contacts.firstIndex(of: contact) {
                            contacts.remove(at: index)
                        }
                    }
                }
            }
            
            group.addToContacts(NSOrderedSet(array: contacts))
            
            group.dateCreated = now
            let formatter = DateFormatter()
            formatter.dateStyle = .long
            formatter.timeStyle = .none
            group.displayName = formatter.string(from: now)
            group.type = 2
        } catch {
            
        }
        return group
    }
    
    static func createCustomGroup(withContext context: NSManagedObjectContext) -> Group {
        let group = Group(context: context)
        group.dateCreated = Date()
        group.displayName = "Untitled"
        group.type = 1
        return group
    }
}

extension NSNotification.Name {
    static var DetailsUpdate = NSNotification.Name("Details Updated")
    static var GroupAdded = NSNotification.Name("New Group")
    static var GroupDeleted = NSNotification.Name("Group Should Be Deleted")
    static var GroupWasDeleted = NSNotification.Name("Group Has Been Deleted")
    static var GroupNameChanged = NSNotification.Name("Group Has Changed Name")
    static var GroupSelected = NSNotification.Name("Group Selection Has Changed")
    static var ContactSelected = NSNotification.Name("Contacts Selection Has Changed")
    static var InsertContact = NSNotification.Name("InsertContactIntoGroup")
    static var ContactAdded = NSNotification.Name("Contact was added to Group")
    static var ContactRemoved = NSNotification.Name("Contact was removed to Group")
    
    static var ActionSent = NSNotification.Name("Action Sent!!")
}
