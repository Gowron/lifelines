//
//  Contact+CoreDataClass.swift
//  Visitation Manager
//
//  Created by Master Design on 6/16/19.
//  Copyright © 2019 Master Design. All rights reserved.
//
//

import Foundation
import CoreData

@objc(Contact)
public class Contact: NSManagedObject {

    @objc dynamic lazy var fullName = { () -> String in
        setLastVisit()
        return "\(self.firstName ?? "???") \(self.lastName ?? "???")"
    }()
    
    func updateName() {
        self.fullName = "\(self.firstName ?? "???") \(self.lastName ?? "???")"
    }
    
    @objc dynamic lazy var statusString = { () -> String in
        switch (self.status) {
        case 1:
            return "Unsaved"
        case 2:
            return "Saved"
        case 3:
            return "Baptized"
        case 4:
            return "Member"
        default:
            return "Unknown"
        }
    }()
    
    func setLastVisit(visited date: Date) {
        self.lastVisited = date
        setNextPrint()
    }
    
    func setLastVisit() {
        var array = visits?.array as! [Visit]
        array.sort(by: {(a, b) -> Bool in
            let dateCompare = a.date!.compare(b.date! as Date)
            switch(dateCompare) {
            case .orderedAscending:
                return true
            case .orderedDescending:
                return false
            default :
                return false
            }
        })
        guard let first = array.first else {return}
        guard let last = array.last else {return}
        let result = first.date!.compare(last.date!) == .orderedDescending
        self.lastVisited = result ? first.date : last.date
        setNextPrint()
    }
    
    func setLastPrint(printed date: Date = Date()) {
        self.lastPrint = date
        setNextPrint()
    }
    
    private func setNextPrint() {
        let result = self.lastVisited!.compare(lastPrint!)
        let date = result == .orderedAscending ? lastPrint! : lastVisited!
        let cal = NSCalendar.current
        var dateComponents = DateComponents()
        dateComponents.month = 3
        var nextDate = cal.date(byAdding: dateComponents, to: date)
        nextDate = cal.date(bySettingHour: 0, minute: 0, second: 0, of: nextDate!)
        if self.nextPrint!.compare(nextDate!) == .orderedAscending {
            self.nextPrint = nextDate
        }
    }
}
