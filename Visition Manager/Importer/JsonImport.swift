//
//  JsonImport.swift
//  Visition Manager
//
//  Created by Master Design on 4/18/19.
//  Copyright © 2019 Master Design. All rights reserved.
//

import Cocoa

class JSONImporter: NSObject {
    
    var managedObjectContext = DataStack.shared.mainContext
    var failedContacts: [Codable] = []
    var importedContacts = 0
    var importedVisits = 0
    
    let file: URL
    let fileName: String
    
    lazy var importGroup: Group = {
        let group = Group(context: managedObjectContext)
        group.dateCreated = Date()
        group.displayName = fileName.lowercased().replacingOccurrences(of: ".json", with: "")
        group.type = 1
        group.displayName = group.displayName?.capitalized
        return group
    }()
    
    init(withFile file: URL) {
        self.file = file
        self.fileName = file.pathComponents.last ?? "NOF"
    }
    
    func fileToText() throws -> String {
        var text = try String(contentsOf: file)
        text = text.replacingOccurrences(of: ",,", with: ", ,")
        return text
    }
    
    func startImport() -> [Codable]{
        do {
            if let data = try fileToText().data(using: .utf8) {
                let decoder = JSONDecoder()
                decoder.dateDecodingStrategy = .iso8601
                let group =  try decoder.decode(JSONGroup.self, from: data)
                importedContacts = group.contacts.count
                for contact in group.contacts {
                    add(contact: contact)
                }
                
            }
        } catch {
            print("errors in decoding")
        }
        return failedContacts
    }
    
    func getContact(ByID id: String) -> Contact {
        if id != "none" {
            do{
                let predicate = NSPredicate.init(format: "serverID equal \(id)")
                let request: NSFetchRequest<Contact> = Contact.fetchRequest()
                request.predicate = predicate
                let contacts = try managedObjectContext.fetch(request)
                if contacts.isEmpty {
                    return Contact.createContact(withContext: managedObjectContext)
                } else {
                    return contacts[0]
                }
            } catch {
            }
        }
        return Contact.createContact(withContext: managedObjectContext)
    }
    
    func getVisit(ByID id: String) -> Visit {
        if id != "none" {
            do{
                let predicate = NSPredicate.init(format: "serverID equal \(id)")
                let request: NSFetchRequest<Visit> = Visit.fetchRequest()
                request.predicate = predicate
                let visits = try managedObjectContext.fetch(request)
                if visits.isEmpty {
                    return Visit(context: managedObjectContext)
                } else {
                    return visits[0]
                }
            } catch {
            }
        }
        return Visit(context: managedObjectContext)
    }
    
    func add(contact: JSONContact) {
        let newContact = getContact(ByID: contact.serverID)
        do {
            newContact.addToGroups(self.importGroup)
            newContact.age = contact.age
            newContact.city = contact.city
            newContact.firstName = contact.firstName
            newContact.lastName = contact.lastName
            newContact.notes = contact.notes
            newContact.phone = contact.phone
            newContact.address = contact.street
            newContact.zipCode = contact.zipCode
            newContact.status = contact.status
            newContact.serverID = contact.serverID
            if let date = contact.lastVisit{
                newContact.lastVisited = date
            }
            try newContact.validateForInsert()
        } catch {
            print("Contact: \(contact.lastName), \(contact.firstName)")
            let nserror = error as NSError
            if nserror.localizedDescription.contains("Multiple validation errors") {
                let alertError = validationError(error: nserror)
                print(alertError.localizedDescription)
            } else {
                print(nserror.localizedDescription)
            }
            managedObjectContext.delete(newContact)
            failedContacts.append(contact)
            importedContacts -= 1
            return
        }
        
        importedVisits += contact.visits?.count ?? 0
        
        for visit in contact.visits ?? [] {
            let newVisit = getVisit(ByID: visit.serverID)
            do {
                newVisit.date = visit.date
                newVisit.notes = visit.notes
                newVisit.visitors = visit.visitedBy
                if (((newContact.visits?.contains(newVisit) ?? false))) {
                } else {
                        newContact.addToVisits(newVisit)
                }
                
                try newVisit.validateForInsert()
            } catch {
                print("Visit: \(visit.date)")
                let nserror = error as NSError
                if nserror.localizedDescription.contains("Multiple validation errors") {
                    let alertError = validationError(error: nserror)
                    print(alertError.localizedDescription)
                } else {
                    print(nserror.localizedDescription)
                }
                managedObjectContext.delete(newVisit)
                failedContacts.append(visit)
                importedVisits -= 1
            }
        }
        
        do {
            try managedObjectContext.save()
        } catch {
            print(error.localizedDescription)
        }
    }
    
    func validationError(error: NSError) -> NSError {
        var description = "Validation Errors:"
        
        let key = "NSDetailedErrors"
        let array = error.userInfo[key] as! NSArray
        
        for item in array {
            let error = item as! NSError
            description.append("\n  -")
            description.append(error.localizedDescription)
            description.append("-")
        }
        var dict = [String: AnyObject]()
        dict[NSLocalizedDescriptionKey] = description as AnyObject?
        dict[NSLocalizedFailureReasonErrorKey] = error.localizedFailureReason as AnyObject?
        dict[NSUnderlyingErrorKey] = error
        return NSError(domain: "com.MasterDesign.Error", code: 9999, userInfo: dict)
    }
}

struct JSONGroup: Codable {
    init(_ contacts : [JSONContact], _ version : Int = 1) {
        self.contacts = contacts
        self.version = version
    }
    var version: Int
    var contacts: [JSONContact]
    
    enum CodingKeys : String, CodingKey {
        case contacts
        case version
    }
}

struct JSONContact: Codable {
    
    init() {}
    
    var serverID: String = "none"
    var age: String = ""
    var city: String = ""
    var firstName: String = ""
    var isActive: Bool = true
    var lastName: String = ""
    var lastVisit: Date?
    var notes: String = ""
    var status: Int16 = 0
    var street: String = ""
    var zipCode: String = ""
    var phone: String = ""
    var visits: [JSONVisit]?
    
    enum CodingKeys : String, CodingKey {
        case age
        case city
        case firstName = "first_name"
        case lastName = "last_name"
        case lastVisit
        case notes
        case phone
        case status
        case street = "address"
        case zipCode
        case isActive
        case visits
        case serverID
    }
}

struct JSONVisit: Codable {
    
    init(_ date: Date) {
        self.date = date
    }
    
    var date: Date
    var notes: String = ""
    var visitedBy: String = ""
    var serverID: String = "none"
    
    enum CodingKeys : String, CodingKey {
        case date
        case notes
        case visitedBy = "visited_by"
        case serverID
    }
}
