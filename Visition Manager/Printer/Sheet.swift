//
//  Sheet.swift
//  Visition Manager
//
//  Created by Master Design on 5/11/19.
//  Copyright © 2019 Master Design. All rights reserved.
//

import Cocoa

class Sheet: NSObject {
    private let defaults = UserDefaults.standard
    let formatter = DateFormatter()
    override init() {
        formatter.dateStyle = .medium
    }
    
    func htmlSheet(forContact contact: Contact, withPageBreak pageBreak: Bool = false) -> String {
        var html = headingView(contact)
        html.append("\n")
        html.append(contactView(contact))
        html.append("\n")
        if let notes = contact.notes {
            if notes != "" {
                html.append(noteView(notes))
                html.append("\n")
            }
        }
        if let visits = contact.visits?.array as? [Visit] {
            let visitsOrdered = visits.sorted { (a, b) -> Bool in
                switch (a.date!.compare(b.date!)) {
                case .orderedAscending:
                    return false
                case .orderedDescending:
                    return true
                default:
                    return true
                }
            }
            
            html.append(visitView(visitsOrdered))
            html.append("\n")
        }
        html.append("\n")
        html.append(footerView())
        if pageBreak {
            html.append("""
            <p class="break"></p>
            """)
        }
        return html
    }
    
    func headingView(_ contact: Contact) -> String {
        let qrSize = 50
        let name = defaults.string(forKey: "church_name")!
        let address = defaults.string(forKey: "church_address")!
        let city = defaults.string(forKey: "church_city")!
        let phone = defaults.string(forKey: "church_phone")!
        
        let qrCode = """
        <img class='barcode' src="https://api.qrserver.com/v1/create-qr-code/?data=https://www.google.com/maps/place/(LOCATION)/;size=\(qrSize)x\(qrSize)"
        title="LOC"
        width="\(qrSize)"
        height="\(qrSize)" />
        """
        let info = """
        <div class="address">
        <h5>\(address)<br/>
        \(city)<br/>
        \(phone)</h5>
        </div>
        """
        
        return """
        <section class="head">
        \(defaults.bool(forKey: "qr_code") ? qrCode : "")
        <h1 class="Church">\(name)</h1>
        <h2 class="City">\(contact.city!)</h2>
        \(defaults.bool(forKey: "info") ? info : "")
        </section>
        """
    }
    
    func contactView(_ contact: Contact) -> String {
        let string = "Unknown"
        let columns = defaults.integer(forKey: "style")
        // TODO: Add Contact State into sheet.
        if columns == 1 {
            return """
            <section class="content">
            <div class="side1">
            <p class='contact'>
            <b>Name:</b> \(contact.firstName ?? string) \(contact.lastName ?? string)<br/>
            <b>Age:</b> \(contact.age ?? string)<br/>
            <b>Phone Number:</b> \(contact.phone ?? string)<br/></p>
            </div>
            <div class="side2">
            <p class='contact'>
            <b>Street Address:</b> \(contact.address ?? string)<br/>
            <b>City:</b> \(contact.city ?? string) \(contact.zipCode ?? string)<br/>
            <b>Salvation:</b> \(contact.statusString)</p>
            </div>
            </section>
            """
        } else if columns == 0 {
            return """
            <section class="content">
            <p class='contact'>
            <b>Name:</b> \(contact.firstName ?? string) \(contact.lastName ?? string)<br/>
            <b>Age:</b> \(contact.age ?? string)<br/>
            <b>Phone Number:</b> \(contact.phone ?? string)<br/>
            <b>Street Address:</b> \(contact.address ?? string)<br/>
            <b>City:</b>  \(contact.city ?? string) \(contact.zipCode ?? string)<br/>
            <b>Salvation:</b> \(contact.statusString)</p>
            </section>
            """
        }
        return """
        <section class="content">
        <div class="side1">
        <p class='contact'>
        <b>Name:</b> \(contact.firstName ?? string) \(contact.lastName ?? string)<br/>
        <b>Age:</b> \(contact.age ?? string)<br/>
        <b>Phone Number:</b> \(contact.phone ?? string)<br/></p>
        </div>
        <div class="side2">
        <p id='contact'>
        <b>Street Address:</b> \(contact.address ?? string)<br/>
        <b>City:</b> (city) \(contact.zipCode ?? string)<br/>
        <b>Salvation:</b> \(contact.statusString)</p>
        </div>
        </section>
        """
    }
    
    func noteView(_ notes: String) -> String {
        return """
        <section class="info">
        <p><b>Information:</b> \(notes)</p>
        </section>
        """
    }
    
    func visitView(_ visits: [Visit]) -> String {
        let limit = defaults.integer(forKey: "visitCount")
        let shouldLimit = defaults.bool(forKey: "limit_visits")
        var count = 0
        var visitNotes = ""
        for visit in visits {
            if visit.date == nil || visit.visitors == nil || visit.notes == nil {
                continue
            }
            visitNotes.append("<b>\(formatter.string(from:  visit.date!))</b> - <b>Visited by:</b> \(visit.visitors!) - <b>Notes:</b> \(visit.notes!)")
            if shouldLimit {
                count += 1
                if limit >= count {
                    return """
                    <section class="visits">
                    <h3><u>Recent Visits</u></h3>
                    <p class="visit">
                    \(visitNotes)
                    </p>
                    </section>
                    """
                }
            }
            if !visit.isEqual(visits.last!) {
                visitNotes.append(" || ")
            }
        }
        return """
        <section class="visits">
        <h3><u>Recent Visits</u></h3>
        <p class="visit">
        \(visitNotes)
        </p>
        </section>
        """
    }
    
    func fieldString() -> String {
        let sun = defaults.bool(forKey: "cd_sun")
        let mon = defaults.bool(forKey: "cd_mon")
        let tue = defaults.bool(forKey: "cd_tue")
        let wed = defaults.bool(forKey: "cd_wed")
        let thu = defaults.bool(forKey: "cd_thur")
        let fri = defaults.bool(forKey: "cd_fri")
        let sat = defaults.bool(forKey: "cd_sat")
        let oth = defaults.bool(forKey: "cd_other")
        var array: [String] = []
        if sun {
            array.append("Sun")
        }
        if mon {
            array.append("Mon")
        }
        if tue {
            array.append("Tue")
        }
        if wed {
            array.append("Wed")
        }
        if thu {
            array.append("Thurs")
        }
        if fri {
            array.append("Fri")
        }
        if sat {
            array.append("Sat")
        }
        if oth {
            array.append("Other")
        }
        
        var days = ""
        
        if !array.isEmpty {
            for day in array {
                if day == array.last {
                    days = "\(days) or \(day);"
                } else {
                    days = "\(days) \(day),"
                }
            }
            return "<b>CIRCLE ONE</b>: \(days)"
        }
        return ""
    }
    
    func footerView() -> String {
        let lines = defaults.integer(forKey: "visit_lines")
        let fields = defaults.integer(forKey: "visit_fields")
        let line = "________________________________________________________________________________________________________________________________<br/>"
        
        var field = """
        <p class="notes"><b>Date:</b> ______/______/______ \(fieldString()) <b>Visited By:</b> _____________________________________________________<br/>
        <b>Notes:</b> __________________________________________________________________________________________________________________________<br/>
        """
        var l = 1
        while l < lines {
            field.append("<br/>")
            field.append(line)
            l += 1
        }
        field.append("</p>")
        
        l = 1
        var footer = """
        <footer>
        \(field)
        """
        while l < fields {
            footer.append(field)
            l += 1
        }
        
        if defaults.bool(forKey: "print_date") {
            footer.append("""
                \n
                <p class="date">\(formatter.string(from: Date()))</p>
                \n
                """)
        }
        footer.append("""
            </footer>
            """)
        return footer
    }
}
