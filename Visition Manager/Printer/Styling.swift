//
//  Styling.swift
//  Visition Manager
//
//  Created by Master Design on 5/3/19.
//  Copyright © 2019 Master Design. All rights reserved.
//

import Foundation

class Style: NSObject {
    private let defaults = UserDefaults.standard
    
    func cssStyle() -> String {
        return """
        \(setTextSize())
        \(headingStyle())
        \(contactStyles())
        \(noteStyle())
        \(visitStyles())
        \(footerStyles())
        """
    }
    
    func setTextSize() -> String {
        // H1 Heading 2x
        // H2 Heading 1.5x
        // H3 Heading 1.17x
        // H4 Heading 1x
        // H5 Heading 0.83x
        // Standard Text 1x
        let chur = defaults.double(forKey: "church_size") / 10
        let city = defaults.double(forKey: "city_size") / 10
        let sel = defaults.double(forKey: "selection_size") / 10
        let adr = defaults.double(forKey: "ch_adr_size") / 10
        let con = defaults.double(forKey: "con_info_size") / 10
        let form = defaults.double(forKey: "form_size") / 10
        return """
        H1 {
        font-size: \(chur)em;
        line-height: 1;
        }
        H2 {
        font-size: \(city)em;
        line-height: 1;
        }
        H3 {
        font-size: \(sel)em;
        text-align: center;
        line-height: 1;
        margin-top: 10px;
        margin-bottom: 10px;
        }
        H5 {
        font-size: \(adr)em;
        text-align: center;
        line-height: 1;
        padding: 0px;
        margin: 2px;
        }
        p {
        font-size: \(con)em;
        line-height: 1;
        }
        .notes {
        font-size: \(form)em;
        }
        section  {
        margin-left: 8px;
        margin-right: 8px;
        }
        """
    }
    

    
    func headingStyle() -> String {
        return """
        .head {
        margin-top: 16px;
        }
        .Church {
        padding-top: 10px;
        margin: auto;
        text-align: center;
        }
        .City {
        text-align: left;
        margin: auto;
        float: left;
        padding-left: 2px;
        margin-top: -15px;
        }
        .address {
        text-align: center;
        width: max-content;
        margin: 0 auto;
        }
        .barcode {
        float: right;
        margin: auto;
        padding-top: 30px;
        padding-right: 30px;
        }
        """
    }

    
    func contactStyles() -> String {
        return """
        .content {
        width: max-content;
        margin: 0 auto;
        }
        .content div {
        display: inline-flex;
        width: max-content;
        margin: 10px 32px 2px 32px
        }
        .contact {
        line-height: 1.3;
        margin-bottom: 0px;
        }
        """
    }
    
    func noteStyle() -> String {
        return """
        .info {
        text-align: center;
        }
        """
    }
    
    func visitStyles() -> String {
        return """
        .visits {
        align-content: center;
        text-align: center;
        }
        p.visit {
        display: inline;
        text-align: center;
        }
        """
    }
    
    func footerStyles() -> String {
        return """
        p.form {
        padding-right: 20px
        padding-left: 20px
        }
        
        p.notes {
        text-align: justify;
        line-height: 1.3;
        }
        footer {
        page-break-before: never;
        display: block;
        padding: 20px 20px;
        content: normal;
        float: none;
        }
        
        .break {
        page-break-after: always;
        }
        
        .date {
        text-align: center;
        }
        """
    }
}
