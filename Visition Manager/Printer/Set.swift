//
//  Set.swift
//  Visitation Manager
//
//  Created by Master Design on 6/11/19.
//  Copyright © 2019 Master Design. All rights reserved.
//

import Foundation

struct PrintSet {
    var contacts: [Contact]
    var date = Date()
    var html: String
    let style = Style()
    let sheet = Sheet()
    
    init(with contacts: [Contact]) {
        self.contacts = contacts.sorted { (a, b) -> Bool in
            let dateCompare = a.city!.compare(b.city!)
            switch(dateCompare) {
            case .orderedAscending:
                return true
            case .orderedDescending:
                return false
            case .orderedSame:
                let nameCompare = a.fullName.compare(b.fullName)
                if nameCompare == .orderedAscending {
                    return false
                }
                return true
            }
        }
        self.html = """
        <!DOCTYPE html>
        <html lang="en">
        <head>
        <style>
        \(style.cssStyle())
        </style>
        </head>
        <body>
        """
        for contact in contacts {
            if contacts.last!.isEqual(contact){
                html.append(sheet.htmlSheet(forContact: contact, withPageBreak: false))
            } else {
                html.append(sheet.htmlSheet(forContact: contact, withPageBreak: true))
            }
        }
        
        html.append("""
        </body>
        </html>
        """)
    }
}
