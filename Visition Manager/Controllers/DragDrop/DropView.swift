//
//  DropView.swift
//  Visition Manager
//
//  Created by Master Design on 5/12/19.
//  Copyright © 2019 Master Design. All rights reserved.
//

import Cocoa

class DropView: NSView {
    
    var image: NSImage?;
    
    override init(frame frameRect: NSRect) {
        super.init(frame: frameRect)
        self.registerForDraggedTypes([.URL])
    }
    
    override func draggingEntered(_ sender: NSDraggingInfo) -> NSDragOperation {
        return NSDragOperation.copy
    }
    
    override func draggingEnded(_ sender: NSDraggingInfo) {
        print(sender.draggingPasteboard.name)
    }
    
    override func draggingExited(_ sender: NSDraggingInfo?) {
        
    }
    
    required init?(coder decoder: NSCoder) {
        super.init(coder: decoder)
        self.registerForDraggedTypes([.URL])
    }
    
    override func draw(_ dirtyRect: NSRect) {
        super.draw(dirtyRect)
    }
}
