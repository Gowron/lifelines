//
//  MainWindow.swift
//  Visition Manager
//
//  Created by Master Design on 4/18/19.
//  Copyright © 2019 Master Design. All rights reserved.
//

//
// TOOLBAR:
// All Toolbar commands are sent to the MainWindow.
// After that they are sent via Notifications to be
// Pocessed.
// Commands that Effect Groups should be pocessed in the group panel.
// If it Effects the Selected Group or Multiply Selected Contacts it
// should be handled in the Contacts List.
// Final, if it effects the singler selected cotnact or visit(s), it
// should be done in the Details Panel.

import Cocoa

class MainWindow: NSWindowController {
    
    @IBOutlet weak var addToButton: NSPopUpButton!
    @IBOutlet weak var removeContact: NSToolbarItem!
    @objc dynamic var canDelete = false
    @objc dynamic var canEdit = true
    let groupMenu = GroupMenu(asToolbar: true)
    let item = NSMenuItem(title: "", action: nil, keyEquivalent: "")
    
    override func windowDidLoad() {
        super.windowDidLoad()
        self.windowTitle(forDocumentDisplayName: "Visitation Manager")
        
        NotificationCenter.default.addObserver(forName: .GroupSelected, object: nil, queue: nil, using: selectedGroupDidChange)
        NotificationCenter.default.addObserver(forName: .ContactSelected, object: nil, queue: nil, using: selectedContactsDidChange)
        NotificationCenter.default.addObserver(forName: GroupMenu.MenuDidChange, object: nil, queue: nil, using: menuChanged(_:))
        self.addToButton.menu = self.groupMenu.getMenu()
        
//        let mainContext = DataStack.shared.mainContext
//        var errorMessage = ""
//        do {
//            let count = try mainContext.count(for: Contact.fetchRequest())
//            errorMessage = "Contacts: \(count)"
//        } catch {
//            _ = error as NSError
//            errorMessage = "error in count: " + error.localizedDescription
//        }
//        let alert = NSAlert()
//        alert.messageText=errorMessage
//        alert.alertStyle = NSAlert.Style.critical
//        alert.runModal()
    }
    
    override func close() {
        NotificationCenter.default.removeObserver(self, name: .GroupSelected, object: nil)
        NotificationCenter.default.removeObserver(self, name: .ContactSelected, object: nil)
        super.close()
    }
    
    func getMenu() -> NSMenu {
        return self.groupMenu.getMenu()
    }
    
    func menuChanged(_ notification: Notification) {
        self.addToButton.menu = getMenu()
    }
    
    func selectedGroupDidChange(_ notification: Notification) {
        guard let id = notification.object as? NSManagedObjectID else { return }
        let context = DataStack.shared.mainContext
        if let group = context.object(with: id) as? Group {
            if group.type != 0 {
                self.canDelete = true
                self.removeContact.label = "Remove Contact"
                self.removeContact.paletteLabel = "Remove Contact"
                self.removeContact.image = NSImage(named: "Remove")
            } else if group.displayName == Group.active {
                self.canDelete = false
                self.removeContact.label = "Archive Contact"
                self.removeContact.paletteLabel = "Archive Contact"
                self.removeContact.image = NSImage(named: "Archive")
            } else if group.displayName == Group.archive {
                self.canDelete = false
                self.removeContact.label = "Activate Contact"
                self.removeContact.paletteLabel = "Activate Contact"
                self.removeContact.image = NSImage(named: "Activate")
            }
        }
    }
    
    func selectedContactsDidChange(_ notification: Notification) {
        guard let count = notification.object as? Int else {
            return
        }
        if count == 1 {
            canEdit = true
        } else {
            canEdit = false
        }
    }
    
    @IBAction func importConacts(_ sender: Any) {
        let panel = NSOpenPanel()
        panel.allowsMultipleSelection = false
        panel.canChooseFiles = true
        panel.canChooseDirectories = false
        panel.allowedFileTypes = ["csv", "CSV", "json", "JSON"]
        panel.begin { (repondence) in
            guard let file = panel.url else { return }
            if file.pathExtension.lowercased() == "json" {
                let importer = JSONImporter(withFile: file)
                let failed = importer.startImport()
                
                NotificationCenter.default.post(name: .GroupAdded, object: importer.importGroup.objectID)
                NotificationCenter.default.post(name: .ContactAdded, object: nil)
                var errorText = ""
                let alert = NSAlert()
                alert.addButton(withTitle: "Done")
                if failed.count >= 1 {
                    errorText = "Some Items Failed to Import!\n"
                    alert.addButton(withTitle: "Manuelly Add")
                    for item in failed {
                        if let contact = item as? JSONContact {
                            errorText.append("Contact: \(contact.firstName) \(contact.lastName), failed to Import\n")
                        } else if let visit = item as? JSONVisit {
                            errorText.append("Visit on \(visit.date), With notes: \(visit.notes), failed to Import\n")
                        }
                    }
                }
                errorText.append(contentsOf: "Successfully Imported: \(importer.importedContacts) Contacts\n\(importer.importedVisits) Visits From File: \(importer.fileName)")
                alert.messageText = errorText
                alert.beginSheetModal(for: self.window!, completionHandler: { (repondence) in
                    if repondence == .alertFirstButtonReturn {
                        
                    } else if repondence == .alertSecondButtonReturn {
                        
                    }
                })
            } else {
            }
            
        }
    }
    
    // MARK: Actions.
    
    // Mark: Group Panel
    @IBAction func deleteGroup(_ sender: Any) {
        NotificationCenter.default.post(name: .GroupDeleted, object: nil)
    }
    
//    @IBAction func newGroup(_ sender: Any) {
//        if let button = sender as? NSMenuItem {
//            NotificationCenter.default.post(name: .GroupAdded, object: button.title)
//        }
//    }
//
//    // Mark: Contact List Panel
//    @IBAction func newContact(_ sender: Any) {
//        NotificationCenter.default.post(name: .NewContact, object: nil)
//    }
//
//    @IBAction func addContactsToGroup(_ sender: Any) {
//        NotificationCenter.default.post(name: .InsertContact, object: nil)
//    }
//
//    @IBAction func removeContacts(_ sender: Any) {
//        NotificationCenter.default.post(name: .ContactRemoved, object: nil)
//    }
//
//    @IBAction func deleteContacts(_ sender: Any) {
//        NotificationCenter.default.post(name: .DeleteContact, object: nil)
//    }
    
    @IBAction func editContact(_ sender: Any) {
        NotificationCenter.default.post(name: .ActionSent, object: Contact.edit)
    }
//
//    @IBAction func printContacts(_ sender: Any) {
//        NotificationCenter.default.post(name: .ContactSelected, object: nil)
//    }
//
//    // Mark: Contact Detail Panel
//    @IBAction func editContact(_ sender: Any) {
//        NotificationCenter.default.post(name: .EditContact, object: nil)
//    }
//
//    @IBAction func newVist(_ sender: Any) {
//        NotificationCenter.default.post(name: .NewVisit, object: nil)
//    }
}
