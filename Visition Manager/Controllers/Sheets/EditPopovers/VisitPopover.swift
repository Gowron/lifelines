//
//  VisitPopover.swift
//  Visition Manager
//
//  Created by Master Design on 4/20/19.
//  Copyright © 2019 Master Design. All rights reserved.
//

import Cocoa

class VisitPopover: NSViewController {
    
    lazy var managedObjectContext = DataStack.shared.childContext
    lazy var visit = {
        return self.representedObject as? Visit
    }()
    
    let format = DateFormatter()
    
    override var representedObject: Any? {
        didSet {
            if let objectID = representedObject as? NSManagedObjectID {
                if let visit = managedObjectContext.object(with: objectID) as? Visit {
                    representedObject = visit
                    if self.isViewLoaded {
                        setFields()
                    }
                }
            }
        }
    }
    
    @IBOutlet weak var VisitDateField: NSTextField!
    @IBOutlet weak var VisitorField: NSTextField!
    @IBOutlet weak var VisitNotesField: NSTextField!
    
    fileprivate func setFields() {
        if let visit = visit {
            self.VisitorField.stringValue = visit.visitors ?? ""
            let date = format.string(from: visit.date ?? Date())
            self.VisitDateField.stringValue = date
            self.VisitNotesField.stringValue = visit.notes ?? ""
            
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.format.dateStyle = .short
        self.format.timeStyle = .none
        setFields()
    }
    
    override func viewWillDisappear() {
        if let visit = visit {
            self.commitEditing()
            visit.date = format.date(from: VisitDateField.stringValue)
            visit.notes = VisitNotesField.stringValue
            visit.visitors = VisitorField.stringValue
            if let contact = visit.contact {
                if visit.date!.compare(contact.lastVisited! as Date) == .orderedDescending {
                    contact.lastVisited = visit.date!
                }
            }
            if managedObjectContext.hasChanges {
                do {
                    try managedObjectContext.save()
                    let notificationCenter = NotificationCenter.default
                    notificationCenter.post(name: .DetailsUpdate, object: visit.objectID)
                } catch {
                    managedObjectContext.rollback()
                }
            }
        }
    }
    
}
