//
//  GroupPopover.swift
//  Visitation Manager
//
//  Created by Master Design on 5/20/19.
//  Copyright © 2019 Master Design. All rights reserved.
//

import Cocoa

class GroupPopover: NSViewController {
    
    @objc dynamic var managedObjectContext = DataStack.shared.childContext
    @objc dynamic var group: Group?
    
    @IBAction func nameAction(_ sender: Any) {
        self.dismiss(self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override var representedObject: Any? {
        didSet {
            if let id = self.representedObject as? NSManagedObjectID {
                if let group = managedObjectContext.object(with: id) as? Group {
                    self.group = group
                }
            }
        }
    }
    
    override func viewWillDisappear() {
        self.commitEditing()
        self.managedObjectContext.commitEditing()
        self.managedObjectContext.trySave()
        NotificationCenter.default.post(name: .GroupNameChanged, object: self.group?.objectID)
    }
    
}
