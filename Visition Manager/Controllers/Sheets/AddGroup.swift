//
//  AddGroup.swift
//  Visition Manager
//
//  Created by Master Design on 4/24/19.
//  Copyright © 2019 Master Design. All rights reserved.
//

import Cocoa

class AddGroup: NSViewController {

    @IBOutlet weak var PopupMenu: NSPopUpButton!
    lazy var groups: [Group] = {
        do {
            let fetchRequest: NSFetchRequest<Group> = Group.fetchRequest()
            fetchRequest.predicate = NSPredicate(format: "type != 0")
            return try DataStack.shared.mainContext.fetch(fetchRequest)
        } catch {
            return []
        }
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        for group in self.groups {
            PopupMenu.addItem(withTitle: group.displayName ?? "ERROR")
        }
    }
    
    override func viewWillDisappear() {
        if let item = PopupMenu.selectedItem {
            for group in groups {
                if group.displayName! == item.title {
                    NotificationCenter.default.post(name: .InsertContact, object: group.objectID)
                }
            }
        }
    }
}
