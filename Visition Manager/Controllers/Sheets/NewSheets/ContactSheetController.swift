//
//  ContactSheetController.swift
//  Visition Manager
//
//  Created by Master Design on 4/11/19.
//  Copyright © 2019 Master Design. All rights reserved.
//

import Cocoa

class ContactSheetController: NSViewController {
    
    @objc dynamic lazy var managedObjectContext = DataStack.shared.childContext
    
    @objc dynamic lazy var contact: Contact = {
        if let id = self.representedObject as? NSManagedObjectID {
            let object = self.managedObjectContext.object(with: id)
            if let contact = object as? Contact {
                self.editing = true
                return contact
            }
        }
       return Contact.createContact(withContext: managedObjectContext)
    }()
    
    override var representedObject: Any? {
        didSet {
            if let objectID = representedObject as? NSManagedObjectID {
                if let contact = managedObjectContext.object(with: objectID) as? Contact {
                    managedObjectContext.rollback()
                    self.contact = contact
                    self.editing = true
                }
            }
        }
    }
    
    @objc dynamic var editing = false
    
    // Labels for Required Fields.
    @IBOutlet weak var firstNameLabel: NSTextField!
    @IBOutlet weak var lastNameLabel: NSTextField!
    @IBOutlet weak var addressLabel: NSTextField!
    @IBOutlet weak var cityLabel: NSTextField!
    @IBOutlet weak var zipCodeLabel: NSTextField!
    
    // Text Fields to check Data.
    @IBOutlet weak var firstNameField: NSTextField!
    @IBOutlet weak var lastNameField: NSTextField!
    @IBOutlet weak var addressField: NSTextField!
    @IBOutlet weak var zipCodeField: NSTextField!
    @IBOutlet weak var cityField: NSTextField!
    @IBOutlet weak var ageField: NSTextField!
    @IBOutlet weak var phoneField: NSTextField!
    @IBOutlet weak var noteField: NSTextField!
    
    @IBAction func cancel(_ sender: Any) {
        if !editing {
            managedObjectContext.delete(contact)
        } else{
            managedObjectContext.discardEditing()
        }
        dismiss(self)
    }
    
    @IBAction func save(_ sender: Any) {
        self.commitEditing()
        managedObjectContext.commitEditing()
        contact.city = contact.city?.capitalized
        contact.firstName = contact.firstName?.capitalized
        contact.lastName = contact.lastName?.capitalized
        do {
            try managedObjectContext.save()
            if editing {
                NotificationCenter.default.post(name: .DetailsUpdate, object: contact.objectID)
            } else {
                NotificationCenter.default.post(name: .ContactAdded, object: contact.objectID)
            }
            dismiss(self)
        } catch {
            let window = NSApplication.shared.mainWindow!
            let nserror = error as NSError
            if nserror.localizedDescription.contains("Multiple validation errors") {
                let alertError = validationError(error: nserror)
                NSAlert(error: alertError).beginSheetModal(for: window) { (response: NSApplication.ModalResponse) in }
            } else {
                NSAlert(error: nserror).beginSheetModal(for: window) { (response: NSApplication.ModalResponse) in }
            }
        }
    }
    
    func validationError(error: NSError) -> NSError {
        var description = "Validation Errors:"
        
        let key = "NSDetailedErrors"
        let array = error.userInfo[key] as! NSArray
        
        for item in array {
            let error = item as! NSError
            description.append("\n  -")
            description.append(error.localizedDescription)
            description.append("-")
        }
        var dict = [String: AnyObject]()
        dict[NSLocalizedDescriptionKey] = description as AnyObject?
        dict[NSLocalizedFailureReasonErrorKey] = error.localizedFailureReason as AnyObject?
        dict[NSUnderlyingErrorKey] = error
        return NSError(domain: "com.MasterDesign.Error", code: 9999, userInfo: dict)
    }
}
