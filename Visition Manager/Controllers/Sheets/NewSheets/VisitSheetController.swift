//
//  VisitSheetController.swift
//  Visition Manager
//
//  Created by Master Design on 4/20/19.
//  Copyright © 2019 Master Design. All rights reserved.
//

import Cocoa

class VisitSheetController: NSViewController {
    
    @objc dynamic lazy var managedObjectContext = DataStack.shared.mainContext
    @objc dynamic lazy var visit = Visit.visit(forContact: self.representedObject as? Contact, withContext: managedObjectContext)
    @objc dynamic lazy var contact = {
        return self.representedObject as? Contact ?? Contact(context: self.managedObjectContext)
    }()
    
    override var representedObject: Any? {
        didSet {
            if let objectID = representedObject as? NSManagedObjectID {
                if let contact = managedObjectContext.object(with: objectID) as? Contact {
                    representedObject = contact
                }
            }
        }
    }
    
    @IBAction func cancel(_ sender: Any) {
        managedObjectContext.delete(visit)
        dismiss(self)
    }
    
    @IBAction func save(_ sender: Any) {
        commitEditing()
        if visit.contact == nil {
            visit.contact = contact
            if let contact = visit.contact {
                if visit.date!.compare(contact.lastVisited! as Date) == .orderedDescending {
                    contact.setLastVisit(visited: visit.date!)
                }
            }
        }
        do {
            try managedObjectContext.save()
            let notificationCenter = NotificationCenter.default
            notificationCenter.post(name: .DetailsUpdate, object: visit.objectID)
            dismiss(self)
        } catch {
            let window = NSApplication.shared.mainWindow!
            let nserror = error as NSError
            if nserror.localizedDescription.contains("Multiple validation errors") {
                let alertError = validationError(error: nserror)
                NSAlert(error: alertError).beginSheetModal(for: window) { (response: NSApplication.ModalResponse) in }
            } else {
                NSAlert(error: nserror).beginSheetModal(for: window) { (response: NSApplication.ModalResponse) in }
            }
        }
    }
    
    func validationError(error: NSError) -> NSError {
        var description = "Validation Errors:"
        
        let key = "NSDetailedErrors"
        let array = error.userInfo[key] as! NSArray
        
        for item in array {
            let error = item as! NSError
            description.append("\n  -")
            description.append(error.localizedDescription)
            description.append("-")
        }
        var dict = [String: AnyObject]()
        dict[NSLocalizedDescriptionKey] = description as AnyObject?
        dict[NSLocalizedFailureReasonErrorKey] = error.localizedFailureReason as AnyObject?
        dict[NSUnderlyingErrorKey] = error
        return NSError(domain: "com.MasterDesign.Error", code: 9999, userInfo: dict)
    }
}
