//
//  GeneralPererences.swift
//  Visitation Manager
//
//  Created by Master Design on 6/2/19.
//  Copyright © 2019 Master Design. All rights reserved.
//

import Cocoa

class GeneralPererences: SettingsViewController {
    
    @IBOutlet weak var sortOrder: NSSegmentedControl!
    let defaults = UserDefaults.standard
    
    @IBOutlet weak var cdSunday: NSButton!
    @IBOutlet weak var cdMonday: NSButton!
    @IBOutlet weak var cdTueday: NSButton!
    @IBOutlet weak var cdWednesday: NSButton!
    @IBOutlet weak var cdThursday: NSButton!
    @IBOutlet weak var cdFriday: NSButton!
    @IBOutlet weak var cdSaturday: NSButton!
    @IBOutlet weak var cdOther: NSButton!
    @IBOutlet weak var statePopup: NSPopUpButton!
    
    var cdArray: [NSButton] = []
    
    override init(nibName nibNameOrNil: NSNib.Name?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        sortOrder.setSelected(true, forSegment: defaults.integer(forKey: "sortOrder"))
        
        cdArray = [cdSunday, cdMonday, cdTueday, cdWednesday, cdThursday, cdFriday, cdSaturday, cdOther]
        
        cdSunday.state = defaults.bool(forKey: "cd_sun") ? .on : .off
        cdMonday.state = defaults.bool(forKey: "cd_mon") ? .on : .off
        cdTueday.state = defaults.bool(forKey: "cd_tue") ? .on : .off
        cdWednesday.state = defaults.bool(forKey: "cd_wed") ? .on : .off
        cdThursday.state = defaults.bool(forKey: "cd_thur") ? .on : .off
        cdFriday.state = defaults.bool(forKey: "cd_fri") ? .on : .off
        cdSaturday.state = defaults.bool(forKey: "cd_sat") ? .on : .off
        cdOther.state = defaults.bool(forKey: "cd_other") ? .on : .off
        
        // TODO: Add Default State to defaults.
        // Pull default State from defaults
        // Set selected state from default state
    }
    
    @IBAction func setDefaultState(_ sender: Any) {
        // TODO: Finish this function
        // TODO: Connect this method to the MenuItem in the pull down list.
        // triggered by MenuItem being selected.
        // set new default state.
    }
    
    @IBAction func SortOrderChanged(_ sender: Any) {
        defaults.set(sortOrder.selectedSegment, forKey: "sortOrder")
    }
    
    @IBAction func action(_ sender: Any) {
        if let button = sender as? NSButton {
            if button.state == .on {
                var count = 0
                
                for button in cdArray {
                    if button.state == .on {
                        count += 1
                    }
                }
                
                if count >= 5 {
                    for button in cdArray {
                        if !(button.state == .on) {
                            button.isEnabled = false
                        }
                    }
                }
            } else {
                for button in cdArray {
                    if button.isEnabled == false {
                        button.isEnabled = true
                    }
                }
            }
            
            switch button.title {
            case "Sunday":
                defaults.set((button.state == .on), forKey: "cd_sun")
                break
            case "Monday":
                defaults.set((button.state == .on), forKey: "cd_mon")
                break
            case "Tuesday":
                defaults.set((button.state == .on), forKey: "cd_tue")
                break
            case "Wednesday":
                defaults.set((button.state == .on), forKey: "cd_wed")
                break
            case "Thursday":
                defaults.set((button.state == .on), forKey: "cd_thur")
                break
            case "Friday":
                defaults.set((button.state == .on), forKey: "cd_fri")
                break
            case "Saturday":
                defaults.set((button.state == .on), forKey: "cd_sat")
                break
            case "Other":
                defaults.set((button.state == .on), forKey: "cd_other")
                break
            default:
                break
            }
        }
    }
}
