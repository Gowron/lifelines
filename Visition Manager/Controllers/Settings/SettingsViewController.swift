//
//  SettingsViewController.swift
//  Visition Manager
//
//  Created by Master Design on 5/10/19.
//  Copyright © 2019 Master Design. All rights reserved.
//

import Cocoa

class SettingsViewController: NSViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Set the size for each view
        self.preferredContentSize = NSMakeSize(self.view.frame.size.width, self.view.frame.size.height)
    }
    
    override func viewDidAppear() {
        super.viewDidAppear()
        
        // Update window title with the active TabView Title
        self.parent?.view.window?.title = self.title!
    }
    
    
}
