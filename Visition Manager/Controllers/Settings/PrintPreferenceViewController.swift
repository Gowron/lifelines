//
//  PrintPreferenceViewController.swift
//  Visition Manager
//
//  Created by Master Design on 5/10/19.
//  Copyright © 2019 Master Design. All rights reserved.
//

import Cocoa

class PrintPreferenceViewController: SettingsViewController {

    @IBOutlet weak var columns: NSSegmentedControl!
    @IBOutlet weak var qrToggle: NSButton!
    @IBOutlet weak var dateToggle: NSButton!
    @IBOutlet weak var infoToggle: NSButton!
    let defaults = UserDefaults.standard
    @IBOutlet weak var noteline: NSTextField!
    @IBOutlet weak var visitFields: NSTextField!
    
    @IBOutlet weak var textSize: NSTextField!
    @IBOutlet weak var chur_size: NSTextField!
    @IBOutlet weak var chur_info: NSTextField!
    @IBOutlet weak var city_size: NSTextField!
    @IBOutlet weak var sel_size: NSTextField!
    @IBOutlet weak var form_size: NSTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        qrToggle.state = defaults.bool(forKey: "qr_code") ? .on : .off
        dateToggle.state = defaults.bool(forKey: "print_date") ? .on : .off
        infoToggle.state = defaults.bool(forKey: "info") ? .on : .off
        columns.setSelected(true, forSegment: defaults.integer(forKey: "style"))
    }
    
    override func viewDidDisappear() {
        if ((Double(textSize.stringValue)) != nil) {
            defaults.set(Double(textSize.stringValue), forKey: "text_size")
        }
        if ((Double(chur_size.stringValue)) != nil) {
            defaults.set(Double(chur_size.stringValue), forKey: "text_size")
        }
        if ((Double(chur_info.stringValue)) != nil) {
            defaults.set(Double(chur_info.stringValue), forKey: "text_size")
        }
        if ((Double(city_size.stringValue)) != nil) {
            defaults.set(Double(city_size.stringValue), forKey: "text_size")
        }
        if ((Double(sel_size.stringValue)) != nil) {
            defaults.set(Double(sel_size.stringValue), forKey: "text_size")
        }
        if ((Double(form_size.stringValue)) != nil) {
            defaults.set(Double(form_size.stringValue), forKey: "text_size")
        }
        if ((Double(noteline.stringValue)) != nil) {
            defaults.set(Double(noteline.stringValue), forKey: "visit_lines")
        }
        if ((Double(visitFields.stringValue)) != nil) {
            defaults.set(Double(visitFields.stringValue), forKey: "visit_fields")
        }
    }
    
    @IBAction func toggleQRCode(_ sender: Any) {
        defaults.set((qrToggle.state == .on), forKey: "qr_code")
    }
    
    @IBAction func toggleShowDate(_ sender: Any) {
        defaults.set((dateToggle.state == .on), forKey: "print_date")
    }
    
    @IBAction func columnSectionChanged(_ sender: Any) {
        defaults.set(columns.selectedSegment, forKey: "style")
    }
    @IBAction func toggleShowInfo(_ sender: Any) {
        defaults.set((infoToggle.state == .on), forKey: "info")
    }
}
