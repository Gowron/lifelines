//
//  masterView.swift
//  Visition Manager
//
//  Created by Master Design on 5/5/19.
//  Copyright © 2019 Master Design. All rights reserved.
//

import Cocoa

class MasterView: NSViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(forName: .DetailsUpdate, object: nil, queue: nil, using: contactChanged(_:))
        NotificationCenter.default.addObserver(forName: .ContactAdded, object: nil, queue: nil, using: contactWasCreated(_:))
        NotificationCenter.default.addObserver(forName: .ContactRemoved, object: nil, queue: nil, using: contactWasDeleted(_:))
        NotificationCenter.default.addObserver(forName: .InsertContact, object: nil, queue: nil, using: contactWasInserted(_:))
        NotificationCenter.default.addObserver(forName: .GroupAdded, object: nil, queue: nil, using: groupWasCreated(_:))
        NotificationCenter.default.addObserver(forName: .GroupDeleted, object: nil, queue: nil, using: groupWasDeleted(_:))
        NotificationCenter.default.addObserver(forName: .GroupNameChanged, object: nil, queue: nil, using: groupWasRenamed(_:))
    }
    
    // Called When a Contact is Edited or a new Visit is added. It is also called when contact is created, deleted, or group list changed
    func contactChanged(_ notification: Notification) -> Void {
    }
    // Called when a new Contact is created.
    func contactWasCreated(_ notification: Notification) -> Void {
    }
    // Called when a new Contact is Deleted from the database.
    func contactWasDeleted(_ notification: Notification) -> Void {
    }
    // Call when a contact is added to a group
    func contactWasInserted(_ notification: Notification) -> Void {
    }
    // Called when a new group is created.
    func groupWasCreated(_ notification: Notification) -> Void {
    }
    // Called when a group is deleted from Database
    func groupWasDeleted(_ notification: Notification) -> Void {
    }
    // Called when a group is renamed.
    func groupWasRenamed(_ notification: Notification) -> Void {
    }
//    
//    override func viewDidDisappear() {
//        NotificationCenter.default.removeObserver(self, name: .DetailsUpdate, object: nil)
//        NotificationCenter.default.removeObserver(self, name: .ContactAdded, object: nil)
//        NotificationCenter.default.removeObserver(self, name: .ContactRemoved, object: nil)
//        NotificationCenter.default.removeObserver(self, name: .InsertContact, object: nil)
//        NotificationCenter.default.removeObserver(self, name: .GroupAdded, object: nil)
//        NotificationCenter.default.removeObserver(self, name: .GroupDeleted, object: nil)
//        NotificationCenter.default.removeObserver(self, name: .GroupNameChanged, object: nil)
//    }
    
}
