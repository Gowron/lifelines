//
//  MainViewController.swift
//  Visition Manager
//
//  Created by Master Design on 4/9/19.
//  Copyright © 2019 Master Design. All rights reserved.
//

import Cocoa
import WebKit

class MainViewController: NSSplitViewController {
    
    @IBOutlet weak var groupsPanel: NSSplitViewItem!
    @IBOutlet weak var detailsPanel: NSSplitViewItem!
    lazy var window = NSApplication.shared.mainWindow
    
    func sendObject(ById id: NSManagedObjectID, ToControllerByName name: String) -> Bool {
        for child in children {
            if child.title == name {
                child.representedObject = id
                return true
            }
        }
        return false
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Visitation Manager"
    }
    
    func displayAlertBox(WithDescription descrip: String, AndFailureReason reason: String) {
        var dict = [String: AnyObject]()
        dict[NSLocalizedDescriptionKey] = descrip as AnyObject?
        dict[NSLocalizedFailureReasonErrorKey] = reason as AnyObject?
        let error = NSError(domain: "com.masterdesign.errors", code: 404, userInfo: dict)
        let alert = NSAlert(error: error)
        if window != nil {
            alert.beginSheetModal(for: window!) { (_ response:NSApplication.ModalResponse) in
                print(response)
            }
        } else  {
            alert.runModal()
        }
    }
    
    @IBAction func newContact(_ sender: Any) {
        let contactSheet = storyboard?.instantiateController(withIdentifier: "contact_sheet") as! NSViewController
        presentAsSheet(contactSheet)
    }
    
    override func toggleSidebar(_ sender: Any?) {
        if let segment = sender as? NSSegmentedControl {
            if segment.selectedCell()?.tag == 0 {
            } else if segment.selectedCell()?.tag == 1 {
            }
        }
    }
    
    override func splitViewDidResizeSubviews(_ notification: Notification) {
        for view in splitViewItems {
            if let sidePanel = view.viewController as? DetailsController {
                sidePanel.setOutliner()
            }
        }
    }
    
    @IBAction func printGroup(_ sender: Any) {
        let groupView = self.groupsPanel.viewController as! SourceController
        let group = groupView.getSelectedGroup()
        let set = PrintSet(with: group.contacts?.array as! [Contact])
        
        let webView = WebView()
        webView.mainFrame.loadHTMLString(set.html, baseURL: nil)
        let when = DispatchTime.now() + 2
        
        DispatchQueue.main.asyncAfter(deadline: when) {
            let printInfo: NSPrintInfo = NSPrintInfo.shared
            printInfo.topMargin = 8.0
            printInfo.leftMargin = 4.0
            printInfo.rightMargin = 4.0
            printInfo.bottomMargin = 4.0
            let printOp: NSPrintOperation = NSPrintOperation(view: webView.mainFrame.frameView.documentView, printInfo: printInfo)
            printOp.showsPrintPanel = true
            printOp.showsProgressPanel = false
            if printOp.run() {
                for contact in set.contacts {
                    contact.setLastPrint()
                }
            }
        }
    }
}
