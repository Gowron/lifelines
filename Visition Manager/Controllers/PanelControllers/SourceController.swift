//
//  SourceController.swift
//  Visition Manager
//
//  Created by Master Design on 4/7/19.
//  Copyright © 2019 Master Design. All rights reserved.
//

import Cocoa

// Side Panel that show a Source List of Groups in Database.
class SourceController: MasterView, NSOutlineViewDelegate {
    
    lazy var window = NSApplication.shared.mainWindow
    // A Link to the Outliner used for the Sidebar.
    // It is used to auto Expand the group after View is redenerd.
    @IBOutlet weak var groupOutliner: NSOutlineView!
    
    // ManagedObjectContext for the CoreData Stack.
    @objc dynamic lazy var managedObjectContext = DataStack.shared.mainContext
    
    // Array of Nodes for the Group Sidebar.
    @objc dynamic lazy var nodes: [Node] = [Node(WithName: "Library", asGroup: true),
                                            Node(WithName: "Custom", asGroup: true),
                                            Node(WithName: "Print", asGroup: true)]
    
    func getSelectedGroup() -> Group {
        if let node = groupOutliner.item(atRow: groupOutliner.selectedRow) as? NSTreeNode {
            if let group = node.representedObject as? GroupNode {
               return managedObjectContext.object(with: group.object) as! Group
            }
        }
        fatalError()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        do{
            try loadGroups()
        } catch {
            let alert = NSAlert(error: error)
            alert.alertStyle = .warning
            if window != nil {
                alert.beginSheetModal(for: window!) { (_ response:NSApplication.ModalResponse) in
                    print(response)
                }
            } else  {
                alert.runModal()
            }
        }
    }

    
    func getIndex(node: GroupNode, ofType type: Int) -> Int {
        if type == 1 {
            let int = nodes[type].children.firstIndex(of: node) ?? 0
            return int + 4
        } else if type == 2 {
            let int = nodes[type].children.firstIndex(of: node) ?? 0
            print(int + 5 + nodes[1].childCount)
            return int + 5 + nodes[1].childCount
        }
        return 1
    }
    
    func getIndex(group: Group, ofType type: Int) -> Int {
        guard type > 0 else { return -1 }
        var i = -1
        for node in nodes {
            i += 1
            for item in node.children {
                i += 1
                let groupNode = item as! GroupNode
                if groupNode.object == group.objectID {
                    return i
                }
            }
        }
        return 1
    }
    
    @IBAction func renameGroup(_ sender: Any) {
        let group = getSelectedGroup()
        let index = getIndex(group: group, ofType: Int(group.type))
        self.SetList(selected: index)
        if index > 0 {
            // New Custom Group!
            let view = groupOutliner.view(atColumn: 0, row: index, makeIfNecessary: true)
            if let view = view {
                let popover = self.storyboard?.instantiateController(withIdentifier: "group_name") as! NSViewController
                popover.representedObject = group.objectID
                self.present(popover, asPopoverRelativeTo: view.visibleRect, of: view, preferredEdge: .maxX, behavior: .semitransient)
            }
        }
    }
    
    override func groupWasCreated(_ notification: Notification) {
        if let id = notification.object as? NSManagedObjectID {
            if let group = self.managedObjectContext.object(with: id) as? Group {
                let node = GroupNode(WithName: group.displayName!, objectID: group.objectID)
                self.nodes[Int(group.type)].add(child: node)
                self.groupOutliner.reloadData()
                let index = getIndex(node: node, ofType: Int(group.type))
                self.SetList(selected: index)
                if group.type == 1 {
                    // New Custom Group!
                    let view = groupOutliner.view(atColumn: 0, row: index, makeIfNecessary: true)
                    if let view = view {
                        let popover = self.storyboard?.instantiateController(withIdentifier: "group_name") as! NSViewController
                        popover.representedObject = group.objectID
                        self.present(popover, asPopoverRelativeTo: view.visibleRect, of: view, preferredEdge: .maxX, behavior: .semitransient)
                    }
                }
            }
        }
    }
    
    override func groupWasDeleted(_ notification: Notification) {
        let id = notification.object as? NSManagedObjectID
        var group: Group?
        if id != nil {
            group = self.managedObjectContext.object(with: id!) as? Group
        }
        if group == nil {
            group = getSelectedGroup()
        }
        for node in nodes {
            for item in node.children {
                if node.name != "Library" {
                    let gNode = item as! GroupNode
                    if group!.objectID == gNode.object {
                        let alert = NSAlert()
                        alert.addButton(withTitle: "Cancel")
                        alert.addButton(withTitle: "Delete")
                        alert.alertStyle = .critical
                        alert.messageText = "Are you sure want to delete \(group?.displayName ?? "") Group?"
                        
                        alert.beginSheetModal(for: NSApplication.shared.mainWindow!) { (response) in
                            if response == .alertSecondButtonReturn {
                                let selected = self.groupOutliner.selectedRow
                                node.remove(child: gNode)
                                self.managedObjectContext.delete(group!)
                                self.groupOutliner.selectRowIndexes(self.setSelectedRow(selected), byExtendingSelection: false)
                                NotificationCenter.default.post(name: .GroupWasDeleted, object: group!.objectID)
                            }
                        }
                    }
                }
            }
        }
    }
    
    func setSelectedRow(_ prev: Int) -> IndexSet {
        var selected = prev
        while (true) {
            if let node = groupOutliner.item(atRow: selected) as? NSTreeNode {
                if (node.representedObject as? GroupNode) != nil {
                    return IndexSet(arrayLiteral: selected)
                } else {
                    selected += 1
                }
            } else {
                selected = 1
            }
        }
    }
    
    override func groupWasRenamed(_ notification: Notification) {
        if let id = notification.object as? NSManagedObjectID {
            if let group = self.managedObjectContext.object(with: id) as? Group {
                let node = nodes[Int(group.type)]
                for item in node.children {
                    let groupNode = item as! GroupNode
                    if groupNode.object == id {
                        groupNode.name = group.displayName!
                        self.groupOutliner.reloadData()
                    }
                }
            }
        }
    }
    
    fileprivate func SetList(selected: Int) {
        self.groupOutliner.expandItem(nil, expandChildren: true)
        self.groupOutliner.selectRowIndexes(IndexSet(arrayLiteral: selected), byExtendingSelection: false)
    }
    
    func loadGroups(selectRow: Int = 1) throws {
        let groups = try managedObjectContext.fetch(Group.fetchRequest()) as! [Group]
        if !groups.isEmpty {
            for group in groups {
                let node = GroupNode(WithName: group.displayName!, objectID: group.objectID)
                nodes[Int(group.type)].add(child: node)
                if group.displayName == "Active Contacts" {
                    if let controller = self.parent as? MainViewController {
                        if !controller.sendObject(ById: group.objectID, ToControllerByName: "Contacts") {
                            controller.displayAlertBox(WithDescription: "Could not Pull up Group Data. Failed to Find Contact List View.",
                                                       AndFailureReason: "Unable to find the Correct View Controller.")
                        }
                    }
                }
                
            }
            
            SetList(selected: selectRow)
        } else {
            
        }
    }
    
    func outlineViewSelectionDidChange(_ notification: Notification) {
        // Called when Selected Item Changes
        let outlineView = notification.object as! NSOutlineView
        if let node = outlineView.item(atRow: outlineView.selectedRow) as? NSTreeNode {
            if let group = node.representedObject as? GroupNode {
                NotificationCenter.default.post(name: .GroupSelected, object: group.object)
                if let controller = self.parent as? MainViewController {
                    if !controller.sendObject(ById: group.object, ToControllerByName: "Contacts") {
                        controller.displayAlertBox(WithDescription: "Could not Pull up Group Data. Failed to Find Contact List View.",
                                                   AndFailureReason: "Unable to find the Correct View Controller.")
                    }
                }
            }
        }
        
    }
    
    func outlineView(_ outlineView: NSOutlineView, shouldSelectItem item: Any) -> Bool {
        let cell = item as! NSTreeNode
        let node = cell.representedObject as! Node
        return !node.group
    }
    
    func outlineView(_ outlineView: NSOutlineView, isGroupItem item: Any) -> Bool {
        let cell = item as! NSTreeNode
        if let node = cell.representedObject as? Node {
            return node.group
        }
        return false
    }
    
    func outlineView(_ outlineView: NSOutlineView, viewFor tableColumn: NSTableColumn?, item: Any) -> NSView? {
        let cell = item as! NSTreeNode
        let node = cell.representedObject as! Node
        var identifier: NSUserInterfaceItemIdentifier
        if node.group {
            identifier = NSUserInterfaceItemIdentifier(rawValue: "HeaderCell")
        } else {
            identifier = NSUserInterfaceItemIdentifier(rawValue: "DataCell")
        }
        let result = outlineView.makeView(withIdentifier: identifier, owner: self) as! NSTableCellView
        result.textField?.stringValue = node.name
        return result
    }
}
