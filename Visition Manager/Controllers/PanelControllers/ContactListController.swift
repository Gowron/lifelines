//
//  ContactListController.swift
//  Visition Manager
//
//  Created by Master Design on 4/7/19.
//  Copyright © 2019 Master Design. All rights reserved.
//

import Cocoa
import WebKit

class ContactListController: MasterView, NSTableViewDelegate {
    
    // This object is the currectly seletect group
    override var representedObject: Any? {
        // did set is called when this value changes
        didSet {
            setContacts()
            guard isViewLoaded else { return }
            guard let group = self.group else { return }
            guard let item = self.tableView.menu?.item(withTag: 6) else { return }
            if group.type != 0 {
                item.title = "Remove"
            } else if group.displayName == Group.active {
                item.title = "Archive"
            } else if group.displayName == Group.archive {
                item.title = "Activate"
            }
            self.tableView.menu!.itemChanged(item)
        }
    }
    
    // ManagedObjectContext for the CoreData Stack.
    @objc dynamic lazy var managedObjectContext = DataStack.shared.mainContext
    @objc dynamic var predicate: NSPredicate?
    @objc dynamic var group: Group?
    @objc dynamic var contacts: [Contact] = []
    @objc dynamic var sorter = [NSSortDescriptor(key: "lastVisited", ascending: true), NSSortDescriptor(key: "city", ascending: true), NSSortDescriptor(key: "lastName", ascending: true)]
    @IBOutlet var arrayController: NSArrayController!
    @IBOutlet weak var tableView: NSTableView!
    
    let groupMenu = GroupMenu(asContextMenu: true)
    
    override func viewDidLoad() {
        NotificationCenter.default.addObserver(forName: .ActionSent, object: nil, queue: nil, using: switcher(_:))
        NotificationCenter.default.addObserver(forName: GroupMenu.MenuDidChange, object: nil, queue: nil) { (notification) in
            guard let item = self.tableView.menu?.item(withTitle: "Add to...") else { return }
            item.submenu = self.groupMenu.getMenu()
            self.tableView.menu?.itemChanged(item)
        }
        NotificationCenter.default.addObserver(forName: UserDefaults.didChangeNotification, object: nil, queue: nil) { (notification) in
            self.sortContacts()
        }
        
        let menu = NSMenu()
        menu.addItem(withTitle: "New Visit", action: #selector(ContactListController.contextMenu(_:)), keyEquivalent: "")
        menu.addItem(withTitle: "Delete", action: #selector(ContactListController.contextMenu(_:)), keyEquivalent: "")
        let remove = NSMenuItem(title: "Archive", action: #selector(ContactListController.contextMenu(_:)), keyEquivalent: "")
        remove.tag=6
        menu.addItem(remove)
        menu.addItem(withTitle: "Edit", action: #selector(ContactListController.contextMenu(_:)), keyEquivalent: "")
        menu.addItem(withTitle: "Print", action: #selector(ContactListController.contextMenu(_:)), keyEquivalent: "")
        let item = NSMenuItem(title: "Add to...", action: nil, keyEquivalent: "")
        item.submenu = groupMenu.getMenu()
        menu.addItem(item)
        tableView.menu = menu
        super.viewDidLoad()
    }
    
    override func viewDidAppear() {
        super.viewDidAppear()
        let alert = NSAlert()
        alert.messageText = "\(String(describing: representedObject))"
        alert.runModal()
    }
    
    @IBAction func contextMenu(_ sender: Any) {
        guard let item = sender as? NSMenuItem else { return }
        arrayController.setSelectionIndex(tableView.clickedRow)
        switch item.title {
        case "New Visit":
            addVisit(sender)
            break
        case "Delete":
            deleteContact(sender)
            break
        case "Archive", "Activate", "Remove":
            removeFromGroup(sender)
            break
        case "Edit":
            editContact()
            break
        case "Print":
            printContact(sender)
            break
        default:
            addToGroup(sender)
            break
        }
    }
    
    // MARK: - PREPARE CONTACT LIST
    // This Method is called when a new Group is selected or a contact has changed.
    private func setContacts(index: Int = 0) {
        if let objectID = representedObject as? NSManagedObjectID {
            group = managedObjectContext.object(with: objectID) as? Group
            contacts.removeAll()
            for item in group!.contacts ?? [] {
                if let contact = item as? Contact {
                    contact.updateName()
                    contacts.append(contact)
                }
            }
            if contacts.count > 0 {
                sortContacts()
                if contacts.count > index {
                    arrayController.setSelectionIndex(index)
                } else {
                    arrayController.setSelectionIndex(contacts.count-1)
                }
                sendSelectedCount()
                if index > 0 {
                    tableView.scrollRowToVisible(tableView.selectedRow)
                }
            }
        }
    }
    
    private func sortContacts() {
        switch UserDefaults.standard.integer(forKey: "sortOrder") {
        case Contact.Sort_Name:
            contacts = contacts.sorted { (a, b) -> Bool in
                let nameCompare = a.lastName!.compare(b.lastName!)
                if nameCompare == .orderedAscending {
                    return true
                }
                return false
            }
            break
        case Contact.Sort_City:
            contacts = contacts.sorted { (a, b) -> Bool in
                let dateCompare = a.city!.compare(b.city!)
                switch(dateCompare) {
                case .orderedAscending:
                    return true
                case .orderedDescending:
                    return false
                case .orderedSame:
                    let nameCompare = a.fullName.compare(b.fullName)
                    if nameCompare == .orderedAscending {
                        return false
                    }
                    return true
                }
            }
            break
        case Contact.Sort_Visit:
            contacts = contacts.sorted { (a, b) -> Bool in
                let dateCompare = a.lastVisited!.compare(b.lastVisited! as Date)
                switch(dateCompare) {
                case .orderedAscending:
                    return true
                case .orderedDescending:
                    return false
                case .orderedSame:
                    let nameCompare = a.fullName.compare(b.fullName)
                    if nameCompare == .orderedAscending {
                        return true
                    }
                    return false
                }
            }
            break
        default:
            break
        }
    }

    func switcher(_ notification: Notification) {
        guard let string = notification.object as? String else { return }
        switch string {
        case Contact.edit:
            editContact()
            break
        default:
            break
        }
    }
    
    func sendSelectedCount() {
        let count = arrayController.selectedObjects.count
        NotificationCenter.default.post(name: .ContactSelected, object: count)
    }
    
    // MARK: - CONTACT
    
    // Called to get the currently Selected Contact. if none selected returns nil
    func getSelectedContact() -> Contact? {
        guard contacts.count > 0 else {
            return nil
        }
        if arrayController.selectedObjects.count < 1 {
            arrayController.setSelectionIndex(0)
        }
        if let contact = arrayController.selectedObjects[0] as? Contact {
            return contact
        }
        return nil
    }
    
    // Called when a Contact Changes.
    override func contactChanged(_ notification: Notification) {
        var i = 0
        if let id = notification.object as? NSManagedObjectID {
            if let contact = managedObjectContext.object(with: id) as? Contact {
                i = contacts.firstIndex(of: contact) ?? 0
                setContacts(index: i)
            } else {
                if let contact = getSelectedContact() {
                    arrayController.setSelectionIndex(contacts.firstIndex(of: contact) ?? 0)
                }
            }
        } else {
            setContacts(index: i)
        }
    }
    
    // MARK: - ADD CONTACT
    
    // Called when a Contact is Created
    override func contactWasCreated(_ notification: Notification) {
        setContacts()
        if let id = notification.object as? NSManagedObjectID {
            if let contact = managedObjectContext.object(with: id) as? Contact {
                arrayController.setSelectionIndex(contacts.firstIndex(of: contact) ?? 0)
            }
        }
    }
    
    override func contactWasInserted(_ notification: Notification) {
        if let id = notification.object as? NSManagedObjectID {
            if let group = self.managedObjectContext.object(with: id) as? Group {
                if let contacts = arrayController.selectedObjects as? [Contact] {
                    for contact in contacts {
                        for cGroup in contact.groups?.allObjects as! [Group] {
                            if cGroup.displayName == group.displayName {
                                return
                            }
                        }
                        group.addToContacts(contact)
                    }
//                    self.managedObjectContext.trySave()
                    tableView.scrollRowToVisible(tableView.selectedRow)
                }
            }
        }
    }
    
    func tableViewSelectionDidChange(_ notification: Notification) {
        sendSelectedCount()
        if let contact = getSelectedContact() {
            if let controller = parent as? MainViewController {
                if !controller.sendObject(ById: contact.objectID, ToControllerByName: "Details") {
                    controller.displayAlertBox(WithDescription: "Could not find Details View", AndFailureReason: "View not found!")
                }
            }
        } else {
            arrayController.setSelectionIndex(0)
        }
    }
    
    // MARK: - MODITIFY CONTACT
    
    /// Shows Add Visit Sheet for Creating a new Visit
    @IBAction func addVisit(_ sender: Any) {
        if let contact = getSelectedContact() {
            let visitSheet = self.storyboard?.instantiateController(withIdentifier: "visit_sheet") as! NSViewController
            visitSheet.representedObject = contact.objectID
            self.presentAsSheet(visitSheet)
        }
    }
    
    /// Shows the Conform Delete ALERT, if delete is selected then deletes
    @IBAction func deleteContact(_ sender: Any) {
        if let contacts = arrayController.selectedObjects as? [Contact] {
            let alert = NSAlert()
            alert.addButton(withTitle: "Cancel")
            alert.addButton(withTitle: "Delete")
            alert.alertStyle = .critical
            if contacts.count == 1 {
                alert.messageText = "Are you sure you want to DELETE: \(contacts[0].fullName) from the Database?"
            } else {
                alert.messageText = "Are you sure you want to DELETE: \(contacts.count) contacts from the Database?"
            }
            alert.beginSheetModal(for: NSApplication.shared.mainWindow!) { (response) in
                if response == .alertSecondButtonReturn {
                    for contact in contacts {
                        self.managedObjectContext.delete(contact)
                    }
                    self.managedObjectContext.trySave()
                    self.setContacts(index: self.arrayController.selectionIndex)
                    
                    self.tableView.scrollRowToVisible(self.tableView.selectedRow)
                }
            }
        }
    }
    
    /// Adds selected contact to selected Group
    @IBAction func addToGroup(_ sender: Any) {
        guard let item = sender as? GroupMenuItem else { return }
        let group = managedObjectContext.object(with: item.objectID!) as! Group
        group.addToContacts(NSOrderedSet(array: arrayController.selectedObjects))
    }
    
    /// Removes selected Contact(s) from Current Group
    @IBAction func removeFromGroup(_ sender: Any) {
        guard let group = group else { return }
        if let contacts = arrayController.selectedObjects as? [Contact] {
            switch group.type {
            case 0:
                for contact in contacts {
                    contact.archive()
                }
                self.managedObjectContext.trySave()
                self.setContacts(index: self.arrayController.selectionIndex)
                return
            default:
                if UserDefaults.standard.bool(forKey: "showRemoveAlert") {
                    let alert = NSAlert()
                    alert.addButton(withTitle: "Cancel")
                    alert.addButton(withTitle: "Remove")
                    alert.alertStyle = .critical
                    
                    if contacts.count == 1 {
                        alert.messageText = "Are you sure you want to remove: \(contacts[0].fullName) from \(group.displayName!)?"
                    } else {
                        alert.messageText = "Are you sure you want to remove: \(contacts.count) contacts from \(group.displayName!)?"
                    }
                    alert.beginSheetModal(for: NSApplication.shared.mainWindow!) { (response) in
                        if response == .alertSecondButtonReturn {
                            self.group?.removeFromContacts(NSOrderedSet(array: contacts))
                            self.managedObjectContext.trySave()
                            self.setContacts(index: self.arrayController.selectionIndex)
                        }
                    }
                } else {
                    self.group?.removeFromContacts(NSOrderedSet(array: contacts))
                    self.managedObjectContext.trySave()
                    self.setContacts(index: self.arrayController.selectionIndex)
                }
                return
            }
        }
        self.tableView.scrollRowToVisible(tableView.selectedRow)
    }
    
    @IBAction func createGroupFromSelected(_ sender: Any) {
        let group = Group.createCustomGroup(withContext: self.managedObjectContext)
        if let contacts = arrayController.selectedObjects as? [Contact] {
            group.addToContacts(NSOrderedSet(array: contacts))
        }
        NotificationCenter.default.post(name: .GroupAdded, object: group.objectID)
    }
    
    func editContact() {
        guard let contact = getSelectedContact() else {return}
        let visitSheet = self.storyboard?.instantiateController(withIdentifier: "contact_sheet") as! NSViewController
        visitSheet.representedObject = contact.objectID
        self.presentAsSheet(visitSheet)
        self.tableView.scrollRowToVisible(tableView.selectedRow)
    }
    
    // MARK: - PRINT CODE
    
    @IBAction func printContact(_ sender: Any) {
        let set = PrintSet(with: arrayController.selectedObjects as! [Contact])
        let webView = WebView()
        webView.mainFrame.loadHTMLString(set.html, baseURL: nil)
        let when = DispatchTime.now() + 2
        
        DispatchQueue.main.asyncAfter(deadline: when) {
            let printInfo: NSPrintInfo = NSPrintInfo.shared
            printInfo.topMargin = 8.0
            printInfo.leftMargin = 4.0
            printInfo.rightMargin = 4.0
            printInfo.bottomMargin = 4.0
            let printOp: NSPrintOperation = NSPrintOperation(view: webView.mainFrame.frameView.documentView, printInfo: printInfo)
            printOp.showsPrintPanel = true
            printOp.showsProgressPanel = false
            if printOp.run() {
                for contact in set.contacts {
                    contact.setLastPrint()
                }
            }
        }
    }
    
    fileprivate func sortContactsByCity(_ contacts: inout [Contact]) {
        contacts = contacts.sorted { (a, b) -> Bool in
            let dateCompare = a.city!.compare(b.city!)
            switch(dateCompare) {
            case .orderedAscending:
                return true
            case .orderedDescending:
                return false
            case .orderedSame:
                let nameCompare = a.fullName.compare(b.fullName)
                if nameCompare == .orderedAscending {
                    return false
                }
                return true
            }
        }
    }
    
    @IBAction func printGroup(_ sender: Any) {
        guard let group = self.group else { return }
        var contacts = group.contacts?.array as! [Contact]
        sortContactsByCity(&contacts)
        let set = PrintSet(with: contacts)
        let webView = WebView()
        webView.mainFrame.loadHTMLString(set.html, baseURL: nil)
        
        let when = DispatchTime.now() + 2
        
        DispatchQueue.main.asyncAfter(deadline: when) {
            let printInfo: NSPrintInfo = NSPrintInfo.shared
            printInfo.topMargin = 8.0
            printInfo.leftMargin = 4.0
            printInfo.rightMargin = 4.0
            printInfo.bottomMargin = 4.0
            let printOp: NSPrintOperation = NSPrintOperation(view: webView.mainFrame.frameView.documentView, printInfo: printInfo)
            printOp.showsPrintPanel = true
            printOp.showsProgressPanel = false
            if printOp.run() {
                for contact in set.contacts {
                    contact.setLastPrint()
                }
            }
        }
    }
    
    @IBAction func saveGroup(_ sender: Any) {
        guard let group = self.group else { return }
        let set = PrintSet(with: group.contacts?.array as! [Contact])
        let savePanel = NSSavePanel()
        savePanel.allowedFileTypes = ["html"]
        savePanel.nameFieldStringValue = group.displayName!
        savePanel.begin { (responce) in
            let file = savePanel.url
            if let file = file {
                do {
                    try set.html.write(to: file, atomically: true, encoding: .utf8)
                } catch {
                    
                }
            }
        }
    }
}
