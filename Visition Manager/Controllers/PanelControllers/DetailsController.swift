//
//  DetailsController.swift
//  Visition Manager
//
//  Created by Master Design on 4/7/19.
//  Copyright © 2019 Master Design. All rights reserved.
//

import Cocoa

class DetailsController: MasterView, NSOutlineViewDelegate {
    
    // ManagedObjectContext for the CoreData Stack.
    @objc dynamic lazy var managedObjectContext = DataStack.shared.mainContext
    
    @objc dynamic var detailArray: [Node] = []
    
    @objc dynamic var contact: Contact?
    
    @objc dynamic var fullName = ""

    @IBOutlet weak var OutlineView: NSOutlineView!
    
    override var representedObject: Any? {
        didSet {
            if let objectID = representedObject as? NSManagedObjectID {
                if let contact = managedObjectContext.object(with: objectID) as? Contact {
                    self.contact = contact
                    setOutliner()
                }
            }
        }
    }
    
    @IBAction func editContact(_ sender: Any) {
        guard let contact = self.contact else {return}
        let visitSheet = self.storyboard?.instantiateController(withIdentifier: "contact_sheet") as! NSViewController
        visitSheet.representedObject = contact.objectID
        self.presentAsSheet(visitSheet)
    }
    
    override func contactChanged(_ notification: Notification) {
        self.setOutliner()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        OutlineView.autosaveExpandedItems = true
        OutlineView.expandItem(nil, expandChildren: true)
    }
    
    fileprivate func rowCount(_ charCount: Int, _ columnWidth: Float) -> Int {
        let charLine = (columnWidth / 10.5).rounded(.down)
        return Int((Float(charCount) / charLine).rounded(.up))
    }
    
    func setOutliner() {
        if let contact = self.contact {
            let formatter = DateFormatter()
            formatter.dateStyle = .medium
            fullName = "\(contact.firstName!) \(contact.lastName!)"
            let Info = Node(WithName: "Print Info", asGroup: true)
            Info.add(child: ContactNode(WithName: "Last Print", AndValue: formatter.string(from: contact.lastPrint!)))
            Info.add(child: ContactNode(WithName: "Next Print", AndValue: formatter.string(from: contact.nextPrint!)))
            let personal = Node(WithName: "Personal", asGroup: true)
            personal.add(child: ContactNode(WithName: "Age", AndValue: contact.age ?? "Unknown"))
            personal.add(child: ContactNode(WithName: "Phone", AndValue: contact.phone ?? "Unknown"))
            personal.add(child: ContactNode(WithName: "Status", AndValue: getStatus(forStatusCode: contact.status)))
            personal.add(child: ContactNode(WithName: "Notes", AndValue: contact.notes ?? "No Notes"))
            let contactNode = Node(WithName: "Contact", asGroup: true)
            contactNode.add(child: ContactNode(WithName: "Address", AndValue: contact.address ?? "Not Listed"))
            contactNode.add(child: ContactNode(WithName: "City", AndValue: contact.city ?? "Unknown"))
            contactNode.add(child: ContactNode(WithName: "Zip Code", AndValue: contact.zipCode ?? "Unknown"))
            let visitsGroup = Node(WithName: "Visits", asGroup: true)
            var visits: [Visit] = contact.visits?.array as! [Visit]
            visits.sort { (a, b) -> Bool in
                let dateCompare = a.date!.compare(b.date! as Date)
                switch(dateCompare) {
                case .orderedAscending:
                    return false
                case .orderedDescending:
                    return true
                default:
                    return false
                }
            }
            for visit in visits {
                let date = formatter.string(from: visit.date!)
                let visitNode = GroupNode(WithName: date, objectID: visit.objectID)
                let visitors = visit.visitors ?? "Unknown"
                let notes = visit.notes ?? "No Notes"
                visitNode.add(child: ContactNode(WithName: "Visitors", AndValue: visitors))
                visitNode.add(child: ContactNode(WithName: "Notes", AndValue: notes))
                visitsGroup.add(child: visitNode)
            }
            detailArray.removeAll(keepingCapacity: true)
            detailArray.append(Info)
            detailArray.append(personal)
            detailArray.append(contactNode)
            detailArray.append(visitsGroup)
        }
        if self.isViewLoaded {
            OutlineView.autosaveExpandedItems = true
            OutlineView.expandItem(nil, expandChildren: true)
        }
    }
    
    func getStatus(forStatusCode code: Int16) -> String {
        switch(code) {
        case 1:
            return "Unsaved"
        case 2:
            return "Saved"
        case 3:
            return "Baptized"
        case 4:
            return "Member"
        default:
            return "Unknown"
        }
    }
    
    func outlineView(_ outlineView: NSOutlineView, shouldSelectItem item: Any) -> Bool {
        let treeCell = item as! NSTreeNode
        if let node = treeCell.representedObject as? Node {
            return (!node.group)
        } else {
            return true
        }
    }
    
    func outlineView(_ outlineView: NSOutlineView, heightOfRowByItem item: Any) -> CGFloat {
        let node = item as! NSTreeNode
        if let node = node.representedObject as? ContactNode {
            let column = outlineView.tableColumns[1]
            let height = rowCount(node.value.count, Float(column.width)) * 18
            if height > 20 {
                return CGFloat(height)
            } else if node.name == "notes"{
                return CGFloat(34)
            }
        }
        return CGFloat(20)
    }
    
    func outlineView(_ outlineView: NSOutlineView, isGroupItem item: Any) -> Bool {
        let cell = item as! NSTreeNode
        if let node = cell.representedObject as? Node {
            return node.group
        }
        return false
    }
    
    func outlineView(_ outlineView: NSOutlineView, viewFor tableColumn: NSTableColumn?, item: Any) -> NSView? {
        let treeNode = item as! NSTreeNode
        let node = treeNode.representedObject as! Node
        var identifier = NSUserInterfaceItemIdentifier(rawValue: "DataCell")
        var result = outlineView.makeView(withIdentifier: identifier, owner: self)
        
        if let node = node as? ContactNode {
            if tableColumn != nil {
                if tableColumn!.identifier.rawValue == "Column 1" {
                    if let result = result as? NSTableCellView {
                        result.textField?.stringValue = node.name
                    }
                } else if tableColumn!.identifier.rawValue == "Column 2" {
                    identifier = NSUserInterfaceItemIdentifier(rawValue: "ValueCell")
                    result = outlineView.makeView(withIdentifier: identifier, owner: self) as! NSTableCellView
                    if let result = result as? NSTableCellView {
                        result.textField?.stringValue = node.value
                    }
                }
            }
        } else {
            if tableColumn != nil && tableColumn!.identifier.rawValue == "Column 1" {
                if let result = result as? NSTableCellView {
                    result.textField?.stringValue = node.name
                }
            } else if tableColumn != nil && tableColumn!.identifier.rawValue == "Column 2" {
                if let group = node as? GroupNode {
                    identifier = NSUserInterfaceItemIdentifier(rawValue: "EditCell")
                    result = outlineView.makeView(withIdentifier: identifier, owner: self) as! NSTableCellView
                    if let button = result?.subviews[0] as? EditButton {
                        button.objectID = group.object
                    }
                }
            } else if node.group {
                identifier = NSUserInterfaceItemIdentifier(rawValue: "HeaderCell")
                result = outlineView.makeView(withIdentifier: identifier, owner: self) as! NSTableCellView
                if let result = result as? NSTableCellView {
                    result.textField?.stringValue = node.name.uppercased()
                }
            }
        }
        return result
    }
    
    @IBAction func editNote(_ sender: Any) {
        if let button = sender as? EditButton {
            let visitSheet = self.storyboard?.instantiateController(withIdentifier: "visit_popover") as! NSViewController
            visitSheet.representedObject = button.objectID
            self.present(visitSheet, asPopoverRelativeTo: button.visibleRect, of: button, preferredEdge: .minX, behavior: .semitransient)
        }
    }
    
    @IBAction func addVisit(_ sender: Any) {
        let visitSheet = self.storyboard?.instantiateController(withIdentifier: "visit_sheet") as! NSViewController
        visitSheet.representedObject = contact?.objectID
        self.presentAsSheet(visitSheet)
    }
}
