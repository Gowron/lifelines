//
//  Node.swift
//  Visition Manager
//
//  Created by Master Design on 4/7/19.
//  Copyright © 2019 Master Design. All rights reserved.
//

import Foundation
import CoreData


// Standard Tree View Node
class Node: NSObject {
    @objc dynamic var name: String
    @objc dynamic let group: Bool
    @objc dynamic var children: [Node] = []
    
    @objc dynamic var childCount: Int {
        return children.count
    }
    
    @objc dynamic var isLeaf: Bool {
        return children.isEmpty
    }
    
    init(WithName name:String, asGroup group:Bool) {
        self.name = name
        self.group = group
    }
    
    init(WithName name:String) {
        self.name = name
        self.group = false
    }
    
    func add(children items:[Node]) {
        for item in items {
            self.children.append(item)
        }
    }
    
    func add(child item:Node) {
        self.children.append(item)
    }
    
    func remove(child item:Node) {
        let index = self.children.firstIndex(of: item) ?? -1
        if index != -1 {
            self.children.remove(at: index)
        }
    }
}

// - MARK: Group Node
class GroupNode: Node {
    @objc dynamic let object: NSManagedObjectID
    
    init(WithName name: String, objectID: NSManagedObjectID) {
        self.object = objectID
        super.init(WithName: name)
    }
}

// - MARK: Contact Node
class ContactNode: Node {
    @objc dynamic let value: String
    
    init(WithName name: String, AndValue value: String) {
        self.value = value
        super.init(WithName: name)
    }
}
