//
//  MDCoreData.swift
//  This file is used in Any Application that needs a Core Data Object
//
//  Created by Judah Eddy on 11/21/18.
//  Copyright © 2018 Judah Eddy. All rights reserved.
//

import Foundation
import CoreData

protocol MDCoreStack {
    var applicationDocumentsDirectory: URL {get}
    var managedObjectModel: NSManagedObjectModel {get}
    var persistentStoreCoordinator: NSPersistentStoreCoordinator {get}
    
    func Fetch<T: NSManagedObject>(type: T.Type, withContext: NSManagedObjectContext) throws -> [T]
    func FetchFilteredList<T: NSManagedObject>(type: T.Type, usingPredicate: NSPredicate?, withContext: NSManagedObjectContext) throws -> [T]
    func getChildContext(ofContext: NSManagedObjectContext) -> NSManagedObjectContext
}

extension MDCoreStack {
    func Fetch<T: NSManagedObject>(type: T.Type, withContext: NSManagedObjectContext) throws -> [T] {
        let fetchRequest: NSFetchRequest<T> = T.fetchRequest() as! NSFetchRequest<T>
        let contacts = try withContext.fetch(fetchRequest)
        return contacts
    }
    
    func FetchFilteredList<T: NSManagedObject>(type: T.Type, usingPredicate: NSPredicate?, withContext: NSManagedObjectContext) throws -> [T] {
        guard let predicate = usingPredicate else {
            let items = try Fetch(type: type, withContext: withContext)
            return items
        }
        let fetchRequest: NSFetchRequest<T> = T.fetchRequest() as! NSFetchRequest<T>
        fetchRequest.predicate = predicate
        let contacts = try withContext.fetch(fetchRequest)
        return contacts
    }
    
    func getChildContext(ofContext: NSManagedObjectContext) -> NSManagedObjectContext {
        let childContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        childContext.parent = ofContext
        return childContext
    }
}

class MDCoreDataBuilder: NSObject, MDCoreStack {
    
    let modelName: String
    let storeType: String
    let fileName: String
    
    // The Application's Documents Directory
    lazy var applicationDocumentsDirectory: URL = {
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return urls[urls.count - 1]
    }()
    
    // This is the Managed Object Model that controls CoreData
    lazy var managedObjectModel: NSManagedObjectModel = {
        let modelURL = Bundle.main.url(forResource: self.modelName, withExtension: "momd")!
        return NSManagedObjectModel(contentsOf: modelURL)!
    }()
    
    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
        let url = self.applicationDocumentsDirectory.appendingPathComponent(self.fileName)
        var failureReason = "There was an error creating or loading the application's saved data."
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)

        do {
            let storeType = self.storeType
            let options = [NSMigratePersistentStoresAutomaticallyOption: true, NSInferMappingModelAutomaticallyOption: true]
            try coordinator.addPersistentStore(ofType: storeType, configurationName: nil, at: url, options: options)
        } catch {
            // Report any Errors
            var dict = [String: AnyObject]()
            dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data" as AnyObject?
            dict[NSLocalizedFailureReasonErrorKey] = failureReason as AnyObject?
            dict[NSUnderlyingErrorKey] = error as NSError
            let wrappedError = NSError(domain: "com.MasterDesign.Error", code: 9999, userInfo: dict)
            NSLog("Unresolved error \(wrappedError) \(wrappedError.userInfo)")
            abort()
        }
        return coordinator
    }()
    
    init(withModelName: String, usingStoreType: String, fileName: String?) {
        self.modelName = withModelName
        self.storeType = usingStoreType
        if let name = fileName {
            self.fileName = name
        } else {
            var defaultName = "coreData"
            switch usingStoreType {
            case NSSQLiteStoreType:
                defaultName += ".sql"
                break
            case NSXMLStoreType:
                defaultName += ".xml"
                break
            default:
                break
            }
            self.fileName = defaultName
        }
    }
    
    convenience init(withModelName: String, usingStoreType:String) {
        self.init(withModelName: withModelName, usingStoreType: usingStoreType, fileName: nil)
    }
}
