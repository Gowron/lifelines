//
//  GroupMenuItem.swift
//  Visitation Manager
//
//  Created by Master Design on 6/10/19.
//  Copyright © 2019 Master Design. All rights reserved.
//

import Cocoa

class GroupMenu: NSObject {
    public static let MenuDidChange = Notification.Name("Group Menu did Change Items.")
    private var menu: NSMenu
    public lazy var context = DataStack.shared.mainContext
    let selector: Selector
    
    func getMenu() -> NSMenu {
        return menu
    }
    
    init(asContextMenu: Bool = false, asToolbar: Bool = false) {
        self.menu = NSMenu(title: "Add to...")
        self.selector = asContextMenu ?#selector(ContactListController.contextMenu(_:)) : #selector(ContactListController.addToGroup(_:))
        super.init()
        if asToolbar {
            let item = NSMenuItem(title: "", action: nil, keyEquivalent: "")
            item.image = NSImage(named: "Insert")
            self.menu.insertItem(item, at: 0)
        }
        let fetchRequest: NSFetchRequest<Group> = Group.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "type != 0")
        do {
            let groups = try context.fetch(fetchRequest)
            for group in groups {
                addGroupToMenu(group)
            }
        } catch {
            
        }
        
        NotificationCenter.default.addObserver(forName: .GroupAdded, object: nil, queue: nil, using: groupAdded(_:))
        NotificationCenter.default.addObserver(forName: .GroupWasDeleted, object: nil, queue: nil, using: groupDeleted(_:))
        NotificationCenter.default.addObserver(forName: .GroupNameChanged, object: nil, queue: nil, using: groupRenamed(_:))
    }
    
    private func addGroupToMenu(_ group: Group) {
        let item = GroupMenuItem(group: group, action: self.selector, keyEquivalent: "")
        menu.addItem(item)
        menu.items.sort { (a, b) -> Bool in
            return a.title.compare(b.title) == .orderedAscending
        }
    }
    
    func groupAdded(_ notification: Notification) {
        guard let group = getGroup(fromNotification: notification) else { return }
        addGroupToMenu(group)
        NotificationCenter.default.post(name: GroupMenu.MenuDidChange, object: group)
    }
    
    func groupRenamed(_ notification: Notification) {
        guard let group = getGroup(fromNotification: notification) else { return }
        guard let items = menu.items as? [GroupMenuItem] else {
            for i in menu.items {
                if let item = i as? GroupMenuItem {
                    if item.objectID == group.objectID {
                        item.title = group.displayName!
                        menu.itemChanged(item)
                    }
                }
            }
            return
        }
        guard !items.isEmpty else { return }
        for item in items {
            if item.objectID == group.objectID {
                item.title = group.displayName!
                menu.itemChanged(item)
            }
        }
        menu.items.sort { (a, b) -> Bool in
            return a.title.compare(b.title) == .orderedDescending
        }
        NotificationCenter.default.post(name: GroupMenu.MenuDidChange, object: group)
    }
    
    func groupDeleted(_ notification: Notification) {
        guard let group = getGroup(fromNotification: notification) else {
            return
        }
        let items = menu.items
        for item in items {
            if let menuItem = item as? GroupMenuItem {
                if menuItem.objectID == group.objectID {
                    menu.removeItem(menuItem)
                }
            }
        }
        NotificationCenter.default.post(name: GroupMenu.MenuDidChange, object: group)
    }
    
    private func getGroup(fromNotification notification: Notification) -> Group? {
        guard let id = notification.object as? NSManagedObjectID else {
            return nil
        }
        guard let group = context.object(with: id) as? Group else {
            return nil
        }
        return group
    }
}

class GroupMenuItem: NSMenuItem {

    public var objectID: NSManagedObjectID?
    
    init(group: Group, action selector: Selector?, keyEquivalent charCode: String) {
        super.init(title: group.displayName!, action: selector, keyEquivalent: charCode)
        self.objectID = group.objectID
    }
    
    required init(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    
}
