//
//  AppDelegate.swift
//  Visition Manager
//
//  Created by Master Design on 4/7/19.
//  Copyright © 2019 Master Design. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {
    
    @objc dynamic var singleContact = true
    @objc dynamic var canDeleteGroup = true
    @objc dynamic var title = "Archive Contact"
    @objc dynamic var tooltip = "Arichive"
    
    

    func applicationDidFinishLaunching(_ aNotification: Notification) {
        // Insert code here to initialize your application
        let defaults: [String : Any] = ["church_name": "Unknown",
                                        "church_address": "Unknown",
                                        "church_city": "Unknown",
                                        "church_phone": "none",
                                        "default_state": "MI",
                                        "qr_code": false,
                                        "print_date": true,
                                        "info": false,
                                        "showRemoveAlert": true,
                                        "limit_visits": false,
                                        "visitCount": 5,
                                        "style": 1,
                                        "sortOrder": 2,
                                        "visit_lines": 4,
                                        "visit_fields": 3,
                                        "church_size": 26,
                                        "city_size": 21,
                                        "selection_size": 17.5,
                                        "ch_adr_size": 13,
                                        "con_info_size": 15,
                                        "form_size": 14]
        
        UserDefaults.standard.register(defaults: defaults)
        NotificationCenter.default.addObserver(forName: .GroupSelected, object: nil, queue: nil, using: selectedGroupDidChange)
        NotificationCenter.default.addObserver(forName: .ContactSelected, object: nil, queue: nil, using: selectedContactsDidChange)
        libraryGroups()
        setupMenu()
    }
    
    func selectedGroupDidChange(_ notification: Notification) {
        guard let id = notification.object as? NSManagedObjectID else { return }
        let context = DataStack.shared.mainContext
        if let group = context.object(with: id) as? Group {
            if group.type != 0 {
                self.canDeleteGroup = true
                self.title = "Remove Contact"
                self.tooltip = "Remove"
            } else if group.displayName == Group.active {
                self.canDeleteGroup = false
                self.title = "Archive Contact"
                self.tooltip = "Archive"
            } else if group.displayName == Group.archive {
                self.canDeleteGroup = false
                self.title = "Activate Contact"
                self.tooltip = "Activate"
            }
        }
    }
    
    func selectedContactsDidChange(_ notification: Notification) {
        guard let count = notification.object as? Int else {
            return
        }
        if count == 1 {
            singleContact = true
        } else {
            singleContact = false
        }
    }
    
    func applicationShouldTerminateAfterLastWindowClosed(_ sender: NSApplication) -> Bool {
        return true
    }
    
    func libraryGroups() {
        do {
            let fetchRequest: NSFetchRequest<Group> = Group.fetchRequest()
            fetchRequest.predicate = NSPredicate(format: "type == 0")
            let groups = try context.fetch(fetchRequest)
            // Check if the the Active Contacts Group Exist.
            if !hasGroup(groups, Group.active)
            {
                // If Group does not Exist
                // Create a Active Contacts Group
                let active = Group(context: context)
                active.displayName = Group.active
                active.dateCreated = Date()
                active.type = 0
                try context.save()
                NotificationCenter.default.post(name: .GroupAdded, object: active.objectID)
            } // Else do nothing
            
            // Check if the Archived Contacts Group Exist
            if !hasGroup(groups, Group.archive)
            {
                // If Group does not Exist
                // Create a Archived Contacts Group
                let archived = Group(context: context)
                archived.displayName = Group.archive
                archived.dateCreated = Date()
                archived.type = 0
                try context.save()
                NotificationCenter.default.post(name: .GroupAdded, object: archived.objectID)
            } // Else do nothing
//
//            // If Groups were Created
//            if context.hasChanges {
//                // Save Core Data State
//                try context.save()
//            }
        } catch {
            let alert = NSAlert(error: error)
            alert.runModal()
        }
    }
    
    func hasGroup(_ groups: [Group], _ group: String) -> Bool {
        return groups.contains(where: { (Group) -> Bool in
            Group.displayName == group
        })
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }

    // MARK: - Core Data stack

    lazy var context = DataStack.shared.mainContext

    // MARK: - Core Data Saving and Undo support

    @IBAction func saveAction(_ sender: AnyObject?) {
        // Performs the save action for the application, which is to send the save: message to the application's managed object context. Any encountered errors are presented to the user.
        if !context.commitEditing() {
            NSLog("\(NSStringFromClass(type(of: self))) unable to commit editing before saving")
        }
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Customize this code block to include application-specific recovery steps.
                let nserror = error as NSError
                NSApplication.shared.presentError(nserror)
            }
        }
    }

    func windowWillReturnUndoManager(window: NSWindow) -> UndoManager? {
        // Returns the NSUndoManager for the application. In this case, the manager returned is that of the managed object context for the application.
        return context.undoManager
    }

    func applicationShouldTerminate(_ sender: NSApplication) -> NSApplication.TerminateReply {
        // Save changes in the application's managed object context before the application terminates.
        if !context.commitEditing() {
            NSLog("\(NSStringFromClass(type(of: self))) unable to commit editing to terminate")
            return .terminateCancel
        }
        
        if !context.hasChanges {
            return .terminateNow
        }
        
        do {
            try context.save()
        } catch {
            let nserror = error as NSError

            // Customize this code block to include application-specific recovery steps.
            let result = sender.presentError(nserror)
            if (result) {
                return .terminateCancel
            }
            
            let question = NSLocalizedString("Could not save changes while quitting. Quit anyway?", comment: "Quit without saves error question message")
            let info = NSLocalizedString("Quitting now will lose any changes you have made since the last successful save", comment: "Quit without saves error question info");
            let quitButton = NSLocalizedString("Quit anyway", comment: "Quit anyway button title")
            let cancelButton = NSLocalizedString("Cancel", comment: "Cancel button title")
            let alert = NSAlert()
            alert.messageText = question
            alert.informativeText = info
            alert.addButton(withTitle: quitButton)
            alert.addButton(withTitle: cancelButton)
            
            let answer = alert.runModal()
            if answer == .alertSecondButtonReturn {
                return .terminateCancel
            }
        }
        // If we got here, it is time to quit.
        return .terminateNow
    }
    
    // MARK: - Groups
    
    @IBAction func newPrintGroup(_ sender: Any) {
        let group = Group.createPrintGroup(withContext: context)
        do {
            try group.validateForInsert()
            context.trySave()
            NotificationCenter.default.post(name: .GroupAdded, object: group.objectID)
        } catch {
            NSAlert(error: error).runModal()
        }
    }
    
    @IBAction func newCustomGroup(_ sender: Any) {
        let group = Group.createCustomGroup(withContext: context)
        NotificationCenter.default.post(name: .GroupAdded, object: group.objectID)
    }
    
    var preferenceController: NSWindowController?
    
    @IBAction func showPerferences(_ sender: Any) {
        if !(self.preferenceController != nil) {
            
            let storyboard = NSStoryboard(name: "Settings", bundle: nil)
            
            preferenceController = storyboard.instantiateInitialController() as? NSWindowController
        }
        
        if self.preferenceController != nil {
            preferenceController?.showWindow(sender)
        }
    }
    
    let groupMenu = GroupMenu()
    @IBOutlet weak var addToItem: NSMenuItem!
    
    func setupMenu() {
        addToItem.submenu = groupMenu.getMenu()
        NotificationCenter.default.addObserver(forName: GroupMenu.MenuDidChange, object: nil, queue: nil) { (notification) in
            self.addToItem.submenu = self.groupMenu.getMenu()
        }
    }
}

