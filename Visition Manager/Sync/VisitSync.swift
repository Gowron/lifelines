//
//  VisitSync.swift
//  Visitation Manager
//
//  Created by Judah M Eddy on 1/25/21.
//  Copyright © 2021 Master Design. All rights reserved.
//

import Cocoa

extension MDSync {
    
    func ShouldCreateNewVisit(_ visit: Visit) -> Bool {
        // Write Logic
        return true
    }
    
    func getData(from visit: Visit) -> [String: String] {
        return [
            "visitors"  : visit.visitors ?? "",
            "notes"     : visit.notes ?? "",
            "date"      : getDate(fromDate: visit.date!)
        ]
    }
    
    func getVisitURL(for visit: Visit) -> String {
        guard let contact = visit.contact else { return "" }
        if let id = contact.serverID {
            if id != "none" {
                return urlBase + urlVisit + "/" + id
            }
        } else {
            onContactShouldSync(contact)
            if let id = contact.serverID {
                if id != "none" {
                    return urlBase + urlVisit + "/" + id
                }
            }
            print("ServerID", contact.serverID ?? "")
        }
        return ""
    }
    
    func onSyncVisit(_ visit : Visit) {
        let data = getData(from: visit)
        if (ShouldCreateNewVisit(visit)){
            let request = createRequest(forURL: getVisitURL(for: visit), using: .POST, withBody: data)
            RunRequest(request)
        } else {
            let request = createRequest(forURL: getVisitURL(for: visit), using: .PUT, withBody: data)
            RunRequest(request)
        }
        
    }
}
