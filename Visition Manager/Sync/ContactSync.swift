//
//  ContactSync.swift
//  Visitation Manager
//
//  Created by Judah M Eddy on 1/23/21.
//  Copyright © 2021 Master Design. All rights reserved.
//

import Cocoa

extension MDSync
{
    private func shouldCreateNewContact() -> Bool {
        // TODO: Add Logic to check Server for Contact.
        return true;
    }
    

    
    func onContactShouldSync(_ contact: Contact){
        
        let data = getData(from: contact)
        
        // Check if the Contact is new or already exists on Database.
        if (shouldCreateNewContact()) {
            let request = self.createRequest(forURL: "", using: .POST, withBody: data)
            RunRequest(request)
        } else {
            let request = self.createRequest(forURL: "", using: .PUT, withBody: data)
            RunRequest(request)
        }
    }
    
    func getData(from contact: Contact) -> [String: Any] {
        return [
            "address":  contact.address ?? "",
            "age":      contact.age     ?? "",
            "city":     contact.city    ?? "",
            "firstName":contact.firstName ?? "",
            "lastName": contact.lastName ?? "",
            "lastPrint":getDate(fromDate: contact.lastPrint!),
            "lastVist": getDate(fromDate: contact.lastVisited!),
            "notes":    contact.notes   ?? "",
            "phone":    contact.phone   ?? "",
            "status":   contact.status,
            "statusStr":contact.statusString,
            "zipCode":  contact.zipCode ?? ""
        ]
    }
    
}
