//
//  MDServerSync.swift
//  Lifelines
//
//  Created by Judah M Eddy on 1/23/21.
//  Copyright © 2021 Master Design. All rights reserved.
//

import Cocoa

class MDSync: NSObject {
    
    // Web Response Codes for the operations
    enum responseCode : Int {
        case ItemCreated = 201
        case AccessDenied = 403
        case Accepted = 202
        case OpterationFailed = 401
    }
    
    // Web HTTP Method Calls.
    enum HTTPMethod : String {
        case PUT    //  This Code is used to Update Existing Data.
        case POST   //  This Code is used to Add new Data.
        case DELETE //  This Code is used to Delete Contacts.
        case GET    //  The Default Code: Used for receiving data
    }
    
    // The Callback for the Response.
    let callback: WebResponseProtocol
    // The Base URL for which the URL is built on
    let urlBase: String
    // The URL ending for the login page.
    private let urlLogin = "/login"
    let urlVisit = "/rest/CONTACT_ID/visits"
    let urlContact = "/rest/contacts"
    
//    let codeCreated = 201
//    let codeAccessDenied = 403
//    let codeAccepted = 202
//    let codeFailed = 401
    
    // Contracter for MDServerSync which requires a callback.
    init(withCallback callback: WebResponseProtocol) {
        self.callback = callback
        // Get the base url from the settings.
        self.urlBase = UserDefaults.standard.string(forKey: "server_url")!
    }
    
    func createRequest(forURL url: String, using method: HTTPMethod, withBody data: [String: Any]) -> URLRequest {
        var request = URLRequest(url: URL(string: url)!)
        if (true) {
            guard let token = UserDefaults.standard.string(forKey: "token") else {
                        callback.onLoginRequired()
                        return request
            }
            request.setValue(token, forHTTPHeaderField: "x-access-token")
        }
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpMethod = method.rawValue
        request.httpBody = data.percentEncoded()
        return request
    }
    
    // Login the User.
    func loginUser(withName name: String, andPassword password: String) {
        let url = URL(string: urlBase + urlLogin)!          //Create a URL from the urlBase and loginURL
        var request = URLRequest(url: url)                  // Create a new URL Request using that URL.
        let auth = String(format: "%@:%@", name, password)  // Send the Name and Password as Auguments in the header
        
        request.setValue("Basic \(auth)",
                         forHTTPHeaderField: "Authorization") // Add the Name and Password to the request.
        
        let task = URLSession.shared.dataTask(with: request) {data, response, error in
            guard let data = data,
                let response = response as? HTTPURLResponse,
                error == nil // Check for no errors.
            else {
                print("error", error ?? "Unknown error") // Display error message.
                return
            }
            
            do {
                var results = try JSONSerialization.jsonObject(with: data, options: []) as! [String: String]
                results["status_code"] = String(response.statusCode)
                self.callback.onResponseReceived(results as NSDictionary)
            } catch {
                print(error.localizedDescription)
            }
            
        }
        task.resume()
    }
    
    func RunRequest(_ request: URLRequest) {
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data,
                  let response = response as? HTTPURLResponse,
                  error == nil else{
                print("error", error ?? "Unknown error")
                return
            }
            
            do {
                var results = try JSONSerialization.jsonObject(with: data, options: []) as! [String: Any]
                results["status_code"] = String(response.statusCode)
                self.callback.onResponseReceived(results as NSDictionary)
            } catch {
                print("")
            }
        }
        task.resume()
    }
    
    func getDate(fromDate: Date) -> String {
        let components = Calendar.current.dateComponents([Calendar.Component.day, Calendar.Component.month, Calendar.Component.year], from: fromDate)
        
        return "day:\(components.day!);month:\(components.month!);year:\(components.year!)"
    }
}

protocol WebResponseProtocol {
    func onResponseReceived(_ json: NSDictionary)
    func onLoginRequired()
}

extension Dictionary {
    func percentEncoded() -> Data? {
        return map { key, value in
            let escapedKey = "\(key)".addingPercentEncoding(withAllowedCharacters: .urlQueryValueAllowed) ?? ""
            let escapedValue = "\(value)".addingPercentEncoding(withAllowedCharacters: .urlQueryValueAllowed) ?? ""
            return escapedKey + "=" + escapedValue
        }
        .joined(separator: "&")
        .data(using: .utf8)
    }
}

extension CharacterSet {
    static let urlQueryValueAllowed: CharacterSet = {
        let generalDelimitersToEncode = ":#[]@" // does not include "?" or "/" due to RFC 3986 - Section 3.4
        let subDelimitersToEncode = "!$&'()*+,;="

        var allowed = CharacterSet.urlQueryAllowed
        allowed.remove(charactersIn: "\(generalDelimitersToEncode)\(subDelimitersToEncode)")
        return allowed
    }()
}

